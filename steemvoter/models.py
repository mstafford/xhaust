from activities.models import Action
from django.db import models

# Create your models here.

class VoteQueue(models.Model):
    author = models.CharField(max_length=20,blank=False) # Steem Author
    permlink = models.CharField(max_length=250,blank=False) # Steem comment perm link
    weight = models.IntegerField(default=100, blank=False) # Steem Vote Weight
    delay = models.IntegerField(default=840, blank=False) # 60s*14m = 840s
    time_added = models.IntegerField(default=0,blank=False) # timestamp for time added to queue. Higher = newer

class Commenter(models.Model):
    activity = models.ForeignKey(Action, on_delete=models.CASCADE)
