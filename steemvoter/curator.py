### This doesn't do anything yet. the intent of this module is to determine the best time & weight of a vote to maximize curation rewards.

from activities.models import Action
from beem.account import Account
from beem.comment import Comment
from django.contrib.auth.models import User

def optimal_voting_time(user):
    account = Account(user.username)
    postList = account.get_blog_entries()
    for post in postList[:20]:
#        print(post['permlink'])
        comment = Comment('@%s/%s' % (user.username,post['permlink']))
        print(comment.get_vote_with_curation('exhaust'))
        votes = comment.get_votes()
        print(votes)
