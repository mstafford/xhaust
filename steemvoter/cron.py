from activities.commenter import sport_summary
from activities.models import Action
from steemvoter.claim import check_RC
from steemvoter.models import Commenter, VoteQueue
from datetime import datetime
from steem.post import Post
from steem import Steem
from beem import Steem as Beem
from beem.account import Account

import re

def voter():
    queueList = VoteQueue.objects.all().order_by('time_added')
    commentList = Commenter.objects.all()
    now = round(datetime.now().timestamp())
    comment_delay = 60
    vote_delay = 850
    weight = 100
    for comment in commentList:
        if now - comment.activity.postdate.timestamp() >= comment_delay:
            sport_summary(comment.activity)
            comment.delete()
    for queue in queueList:
        if now - queue.time_added >= vote_delay:
            post=Post("@"+queue.author+"/"+queue.permlink)
            post.upvote(weight,'exhaust')
            queue.delete()
#            print("Voted on a post by @"+post.author)

def commentvoter():
    account = Account('exhaust')
    VP = account.get_voting_power()
    weekago = datetime.now().timestamp() - 7*3600*24
#    latest = Action.objects.filter(postdate>=weekago)
    if VP >= 92.0:
        print("We should be adding a comment into the vote queue here!")

def pay_delegators():
    s = Steem()
    b = Beem()
    exhaust = Account('exhaust')
    curation_rewards = exhaust.curation_stats()['24hr']
    print(curation_rewards)
    history = exhaust.get_account_history(-1, 10000)
    delegators = {}
    now = datetime.now().timestamp()
    valid_delsum = 0.0
    all_delegations = {}
    current_delsum = 0.0
    for hist in history:
        if "delegator" in hist.keys():
            if hist['delegator'] in all_delegations.keys():
                continue
#            print("Found delegator!")
            timestamp = datetime.strptime(hist['timestamp'],'%Y-%m-%dT%H:%M:%S').timestamp()
            all_delegations[hist['delegator']]={}
            all_delegations[hist['delegator']]['amount']=hist['vesting_shares']['amount']
            all_delegations[hist['delegator']]['date']=timestamp
            all_delegations[hist['delegator']]['active']='no'
    print("ALL DELEGATIONS FOUND:")
    print("==============================")
    for delegator in all_delegations:
        print(delegator + ": " + str(b.vests_to_sp(float(all_delegations[delegator]['amount'])/1000000)))
        current_delsum += float(all_delegations[delegator]['amount'])
    history = exhaust.get_account_history(-1,10000)
    for hist in history:
        if "delegator" in hist.keys():
#            print("Found delegator!")
            timestamp = datetime.strptime(hist['timestamp'],'%Y-%m-%dT%H:%M:%S').timestamp()
            if timestamp > now-3600*24*7:
#                print("Delegation too new... Looking for older state for user " + hist['delegator'])
                continue
            if hist['delegator'] in delegators.keys():
                continue
            delegators[hist['delegator']]={}
            delegators[hist['delegator']]['amount']=hist['vesting_shares']['amount']
            delegators[hist['delegator']]['date']=timestamp
            delegators[hist['delegator']]['active']='yes'
    print("DELEGATIONS VALID FOR PAYMENT:")
    print("=============================")
    for delegator in delegators:
        if delegators[delegator]['active'] == 'yes':
            print(delegator + ": " + str(b.vests_to_sp(float(delegators[delegator]['amount'])/1000000)))
            valid_delsum+=float(delegators[delegator]['amount'])
#    print("====================================")
#    print("TOTAL AMOUNT DELEGATED = " + str(delsum))
    total = float(re.sub(' VESTS','',str(exhaust.get_vests())))*1000000
    total = valid_delsum
    delegated_vests = current_delsum/1000000
    valid_vests = valid_delsum/1000000
    total_vests = exhaust.get_vests()
    exhaust_owned_vests = total_vests - delegated_vests
    active_payment_vests = exhaust_owned_vests + valid_vests
    print("@EXHAUST SUMMARY:")
    print("===========================")
    print("Total Delegated Steem Power: " + str(b.vests_to_sp(delegated_vests)))
    print("Valid Delegated Steem Power: " + str(b.vests_to_sp(valid_vests)))
    print("Total Steem Power: " + str(b.vests_to_sp(total_vests)))
    print("@exhaust's Steem Power: " +str(b.vests_to_sp(exhaust_owned_vests)))
    print("Active Steem Power: " + str(b.vests_to_sp(active_payment_vests)))
    for delegator in delegators:
        delegators[delegator]['percent']=float(delegators[delegator]['amount'])/float(active_payment_vests*1000000)
        usershare = round(delegators[delegator]['percent']*float(curation_rewards),3)
#        print(delegator +  ": " +  str(delegators[delegator]['percent']) +", " +  str(usershare))
#        print(delegator + " has earned " + str(usershare) + " from yesterday")
        memo="Thanks for delegating to Exhaust! You have delegated " + str(round(b.vests_to_sp(float(delegators[delegator]['amount'])/1000000),3)) + " SP! Here is your portion of the curation rewards earned yesterday!"
#        print(memo)
        s.transfer(delegator, amount = usershare, asset='STEEM', memo=memo, account='exhaust')
#    print(delegators)

    def claim_account():
        check_RC()
