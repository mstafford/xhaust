from django.apps import AppConfig


class SteemvoterConfig(AppConfig):
    name = 'steemvoter'
