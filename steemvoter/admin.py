from django.contrib import admin

from .models import Commenter, VoteQueue
# Register your models here.

admin.site.register(Commenter)
admin.site.register(VoteQueue)
