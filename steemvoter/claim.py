from beem.account import Account
from beem.rc import RC
from beem.steem import Steem

def check_RC():
    exhaust = Account('exhaust')
    curr_rc = exhaust.get_rc_manabar()['current_mana']
    rc = RC()
    curr_cost = rc.claim_account()
    s = Steem()
    s.wallet.unlock("WhatwouldyoudoifIsangoutoftune?")
    print(("Claiming an account currently requires %d RC's, and you have %d RC's" % (curr_cost, curr_rc)))
    if curr_rc >= curr_cost:
        s.claim_account('exhaust',fee="0 STEEM")
