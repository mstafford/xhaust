from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.gis import admin
from django.urls import include, path
import steemconnect_auth

from app import views as app_views

urlpatterns = [
    path('', include('activities.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^done/$', app_views.done, name='done'),
    url(r'^markdownx/', include('markdownx.urls')),
    url(r'^accounts/', include('steemconnect_auth.urls')), 
    url(r'^world/', include('world.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
