class MapRouter:
    """
    A router to control all database operations on models in the
    world application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read world models go to worldmap.
        """
        if model._meta.app_label == 'world':
            return 'worldmap'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write world models go to worldmap.
        """
        if model._meta.app_label == 'world':
            return 'worldmap'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the activities app is involved.
        """
        if obj1._meta.app_label == 'activities' or \
           obj2._meta.app_label == 'activities':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the world app only appears in the 'worldmap'
        database.
        """
        if app_label == 'world':
            return db == 'worldmap'
        return None
