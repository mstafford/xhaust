from django.conf.urls import url, include
from django.urls import path

from . import views

urlpatterns = [
    path('check_in/<str:activity_type>/', views.check_in, name='check_in'),
    path('find_crag/<str:lat>/<str:lon>/', views.find_crag, name='find_crag'),
]
