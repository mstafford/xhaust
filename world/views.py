from .models import WorldBorder, Province, SubRegion, Crag

from django.contrib.auth.models import User
from django.contrib.gis.measure import D
from django.contrib.gis.geos import Point

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template import loader

def find_crag(request, lat, lon):
    template = loader.get_template('world/navigator.html')
#    pnt = 'POINT('+lon+' '+lat+')'
    pnt = Point(float(lon),float(lat))
    crags = Crag.objects.filter(mpoly__distance_lte=(pnt,D(km=3500)))
    for crag in crags:
        crag.distance = crag.mpoly.distance(pnt)
    crags.order_by('distance')
    closest = crags[0]
    close_dist = closest.mpoly.distance(pnt)*100
    distList = []
    for crag in crags:
        distList.append([crag.name, crag.mpoly.distance(pnt)*100])
    context = {
    "LON":lon,
    "LAT":lat,
    "CRAGNAME":closest.name,
    "DISTANCE":round(close_dist,2),
    "DISTLIST":distList,
    }
    return HttpResponse(template.render(context,request))

def check_in(request, activity_type):
    user = User.objects.get(username=request.user)
    template = loader.get_template('world/checkin.html', using='backend')
    context = {
    }
    
    return HttpResponse(template.render(context,request))
# Create your views here.
