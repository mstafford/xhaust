from django.contrib.gis import admin
from .models import WorldBorder, Province, SubRegion
from .models import Crag

admin.site.register(WorldBorder, admin.GeoModelAdmin)
admin.site.register(Province, admin.GeoModelAdmin)
admin.site.register(SubRegion, admin.GeoModelAdmin)
admin.site.register(Crag, admin.GeoModelAdmin)

# Register your models here.
