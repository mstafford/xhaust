import json

from activities.models import User

from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout, login
from django.utils import timezone

from steemconnect.operations import Vote, Comment
from steemconnect.client import Client
from social_core.backends.oauth import BaseOAuth1, BaseOAuth2
from social_core.backends.google import GooglePlusAuth
from social_core.backends.utils import load_backends
from social_django.utils import psa, load_strategy
from social_django.models import UserSocialAuth

from .decorators import render_to


def logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('/')


@render_to('home.html')
def home(request):
    """Home view, displays login mechanism"""
    print(request.user)
    if request.user.is_authenticated:
        #Check if user is registered with EXHAUST
        return redirect('/dash')


@login_required
@render_to('home.html')
def done(request):
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CHECKING TO SEE IF USER EXISTS IN EXHAUST DATABASE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print(request.user)
    user = (request.user)
    account_list = User.objects.all()
    search = 0
    if len(account_list) > 0:
        for account in account_list:
            print(user, account.username)
            if str(user) == str(account.username):
                print("++++++++++++++++++FOUND A RETURNING RUNNER!!!+++++++++++++++++++")
                search = 1
                break
    if search == 0:
        print("============================Trying to add new account===================================")
        new_account = User(username=user, nickname='FunRunner', join_date = timezone.now())
        new_account.save()
        
    print(user)
#    for 
    
    """Login complete view, displays user data"""
    return redirect('/')

