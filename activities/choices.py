MAP_PRIVACY_CHOICES = (
    (1, ("Don't Share")),
    (2, ("Friends Only")),
    (3, ("People I follow")),
    (4, ("My Followers")),
    (5, ("Share with All"))
)
