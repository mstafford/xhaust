# Generated by Django 2.1 on 2018-09-15 22:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0020_auto_20180902_2036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='run',
            name='comments',
            field=models.TextField(blank=True, max_length=2048),
        ),
    ]
