# Generated by Django 2.1 on 2018-08-09 14:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0008_auto_20180809_1352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='run',
            name='uploaded_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
