from django.contrib.auth.models import User

from activities.models import Action, activityType, LeaderBoard
from activities.secret import walletunlock
from beem.comment import Comment
from beem import Steem
from datetime import datetime


def sport_summary(activity):
    now = datetime.now().timestamp()
    user = activity.user
    acttype = activity.type
    total_distance = 0.0
    total_duration = 0.0
    acts = Action.objects.filter(user=user, type=acttype)
    latest = acts.last()
    similar_acts = acts.filter(distance__gte=latest.distance*0.9, distance__lte=latest.distance*1.1)
    similar_acts = similar_acts.order_by('pace')
    xhstComment = ("You have uploaded %d %s activities, and %d of them have been of similar distances (within 10 percent)!\n" % (len(acts),acttype.type,len(similar_acts)))
    xhstComment += ("Here are your 5-fastest %s activities of a similar distance:\n" % (acttype.type))
    xhstComment += "\n"
    for act in similar_acts[:5]:
        xhstComment += " - " + act.title + " - " + str(act.distance/1000) + "km @ " + act.pace + " min/km pace.\n"
    for act in similar_acts:
        total_distance += act.distance/1000
        total_duration += act.duration/60
    avg_pace = (total_duration/total_distance)
    average_pace = str(int(avg_pace)) + ":" + str(int((avg_pace-int(avg_pace)) *60))

    xhstComment += ("\nOver these %d similar activities, you have travelled approximately %.2f kms at an average pace of roughly %s min/km!\n" % (len(similar_acts),total_distance, average_pace))
    latestpace = (latest.duration/60)/(latest.distance/1000)
    print(latestpace, avg_pace)
    if latestpace >= avg_pace:
        xhstComment+="\nLooks like you went a bit slower this time! Remember that it's important to take some rest days, and do some rehab / prehab / strength and conditioning training too!"
    else:
        xhstComment+="\nLooks like you went a bit faster than usual this time! Nice work!"
    print(xhstComment)
    steem = Steem()
    comment = Comment(activity.permlink, steem_instance=steem)
    steem.wallet.unlock(walletunlock)
    comment.reply(xhstComment, title=comment.title + " - XHST Review",author="exhaust")
    print("Posted?")
#    print(latest.title, latest.distance)
#    print(similar_acts)
