from activities.models import Action, activityType, Event, Result
from beem.amount import Amount
from beem.comment import Comment
from datetime import datetime

def closed_event_summary(event):
    summary = ''
    usersummary = ''
    rewards = Amount(0,asset='SBD')
    event_summary = {}
    now = datetime.now()
    event_summary['exhaust'] = {'rewards':rewards}
    summary += ("Summary for Event: %s - %s\n***\n" % (event.name, datetime.strftime(event.event_date,'%B %d, %Y')))
    for user in event.participants.all():
        event_summary[user.username] = {'distance':0,'count':0, 'contrib':Amount(0,asset="SBD")}
        useracts = Action.objects.filter(user=user, postdate__gte=event.create_date, posted=True, type=event.event_type)
        for act in useracts:
            event_summary[user.username]['distance']+=round(act.distance/1000,3)
            event_summary[user.username]['count'] += 1
            comment = Comment(act.permlink)
            postrewards = comment.get_author_rewards()['total_payout_SBD']*event.event_tax
            event_summary[user.username]['contrib'] += postrewards
            rewards += postrewards
        if event_summary[user.username]['contrib'] <= Amount(0, asset='SBD'):
            continue
        usersummary += ("#### User @%s:\n\n * They've logged %d runs since signing up; and\n * covered roughly %.2f km's!; and\n * contributed roughly %s to the prize-pool!\n\n" % (user.username, event_summary[user.username]['count'],event_summary[user.username]['distance'],event_summary[user.username]['contrib']))
    summary += "# Generated Prize-Pool of %s... but we're gonna bump it up to 20.000 SBD as a way of saying thanks to everyone for using and giving feedback on EXHAUST!\n" % (event_summary['exhaust']['rewards'])
    summary += "## Payout Structure: 1st - 60%, 2nd - 25%, 3rd - 10%, 4th - 5%\n***\n"
    summary += "### Made possible by the following runners:\n"
    summary += usersummary
    event_summary['exhaust']['summary'] = summary
    print(event_summary)
    print(event_summary['exhaust']['summary'])
