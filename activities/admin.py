from django.contrib import admin

from .models import Action, Event, EventPost, User, LeaderBoard
from .models import Result, Statistic, activityType, xhstSettings

# Register your models here.

admin.site.register(Action)
admin.site.register(activityType)
admin.site.register(Event)
admin.site.register(EventPost)
admin.site.register(LeaderBoard)
admin.site.register(Result)
admin.site.register(Statistic)
admin.site.register(User)
admin.site.register(xhstSettings)
