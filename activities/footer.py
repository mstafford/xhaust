from beem.account import Account
from beem.comment import Comment
from bs4 import BeautifulSoup
from datetime import datetime
from markdown2 import Markdown

def update_footer():
    htmlfile = open('common/templates/footer.html','r')
    account = Account('exhaust')
    blogList = []
    counter = 1
    now = datetime.now()
    for post in account.get_blog_entries():
        if post['author'] == 'exhaust':
            blogList.append(post['permlink'])
    print(blogList)
    soup = BeautifulSoup(htmlfile,'html.parser')
    counter = 0
    htmlfile = open('common/templates/footer.html','w')
    links = soup.find_all('a')
    spans = soup.find_all('span')
    descrips = soup.find_all('p')
    months = soup.find_all('b')
    markdowner = Markdown()
    calendar = {
        "1":"JAN",
        "2":"FEB",
        "3":"MAR",
        "4":"APR",
        "5":"MAY",
        "6":"JUN",
        "7":"JUL",
        "8":"AUG",
        "9":"SEP",
        "10":"OCT",
        "11":"NOV",
        "12":"DEC",
}
    for i in range(0,3):
        link = links[i] #href links found in soup
        span = spans[i] #"spans" found in soup
        monthtag = months[i] #month "p" tag in soup
        descrip = descrips[i]
        spanmonth = span.next
        comment = Comment('@exhaust/'+str(blogList[i]))
        postBody = markdowner.convert(comment.body)
        postSoup = BeautifulSoup(postBody,'html.parser')
        postTime = comment.time_elapsed()
        then = now - postTime
        month = str(then.month)
        day = str(then.day)
        link.string = comment.title
        span.strong.string = day
        monthtag.string = calendar[month]
        firstp = postSoup.find('p')
        descrip.string = firstp.text[:80]+str("...")
        link['href'] = "/article/@exhaust/" +str(blogList[i])
        print(link, span, descrip)
    htmlfile.write(soup.prettify())
    htmlfile.close()
