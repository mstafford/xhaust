import PIL
from PIL import Image

#def downscale(photo,path,rename):
#    basewidth = 640
#    img = Image.open('media/'+photo)
#    wpercent = (basewidth / float(img.size[0]))
#    hsize = int((float(img.size[1])*float(wpercent)))
#    img = img.resize((basewidth,hsize), PIL.Image.ANTIALIAS)
#    img.save('media/' + path + rename)

def downscale(photo,path,rename):
    if '.gif' in photo:
        pass
    else:
        basewidth = 640
        img = Image.open('media/'+photo)
        rgb_img = img.convert('RGB')
        wpercent = (basewidth / float(rgb_img.size[0]))
        hsize = int((float(rgb_img.size[1])*float(wpercent)))
        rgb_img = rgb_img.resize((basewidth,hsize), PIL.Image.ANTIALIAS)
        rgb_img.save('media/' + path + rename + '.jpg')
    print("It should have worked!!!")
