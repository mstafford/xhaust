from tcxparser import TCXParser
from datetime import datetime
from activities.models import Run

runList = Run.objects.all()

for run in runList:
    if run.gpsfile:
        tcx = TCXParser('media/'+str(run.gpsfile))
        runDate = tcx.completed_at
        run.runStart = datetime.strptime(runDate, "%Y-%m-%dT%H:%M:%SZ")
        run.save()
        print("Cleaned run: " + str(run.title))