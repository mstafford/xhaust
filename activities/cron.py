from activities.autoposter import newsletter as get_newsletter
from activities.models import Statistic, LeaderBoard, activityType, Event, Result
from activities.models import Action as Activity
from activities.views import check_tax
from beem.account import Account
from beem.comment import Comment
from django.contrib.auth.models import User

from datetime import datetime
from steem import Steem
from beem import Steem as Beem
import re
import time

def daily_newsletter():
    newsletter = get_newsletter()
    today = datetime.today()
    title = "State of Exhaustion - " + today.strftime("%B %d, %Y")
    tags = ['exhaust','fitnation','running','cycling','training']
    steem = Beem()
    steem.post(title=title, body=newsletter, author='exhaust', tags=tags)

def weekly_payout():
    b = Beem()
    s = Steem()
    account = Account('exhaust')
    rewardbalance = account.get_balances()['rewards'][1]
    rewardbalance = float(re.sub(' SBD','',str(rewardbalance)))
    print("Avaialble rewards = " + str(rewardbalance)+ " SBD!!")
    price = b.get_median_price()
    price = float(re.sub(' STEEM/SBD','',str(price.invert())))
    print(price)
    actList = Activity.objects.filter(posted="True").order_by('postdate').reverse()
    payList = []
    now = datetime.now().timestamp()
    oneweek = now - 60*60*24*7
    twoweek = now - 60*60*24*14
    total_run_distance = 0.0
    total_bike_distance = 0.0
    total_hike_distance = 0.0
    total_yoga_duration = 0.0
    userList = User.objects.all()
    s.claim_reward_balance(account='exhaust')
    runsum = 0.0
    bikesum = 0.0
    hikesum = 0.0
    yogasum = 0.0
    benefsum = 0.0
    blogsum = 0.0
    commentsum = 0.0
    for user in userList:
        print(user.username)
        try:
            stats = LeaderBoard.objects.get(user=user)
        except:
            stats = LeaderBoard(user=user)
            stats.save()
        stats.pay_distance_run = 0.0
        stats.pay_distance_bike = 0.0
        stats.pay_distance_hike = 0.0
        stats.pay_duration_yoga = 0.0
        stats.save()

    for act in actList:
        if act.postdate.timestamp() <= oneweek and twoweek <= act.postdate.timestamp():
            if act.pk == 422:
                continue;
            if str(act.user.username) not in payList: #If user hasn't been paid before, add to list.
                payList.append(str(act.user.username))
            post = Comment(act.permlink) #Find Steem post for activity
            rewards = str(post.get_rewards()['author_payout'])   #See how much author was rewarded for activity
            user = act.user
            stats = LeaderBoard.objects.get(user=user)
            if act.type.type == 'running': #Allot to running pool
                runsum += float(re.sub(' SBD','',rewards))
                stats.pay_distance_run += round(act.distance/1000,3)
                stats.save()
                total_run_distance += round(act.distance/1000,3)
            if act.type.type == 'cycling': #Allot to cycling pool
                bikesum += float(re.sub(' SBD','',rewards))
                stats.pay_distance_bike += round(act.distance/1000,3)
                stats.save()
                total_bike_distance += round(act.distance/1000,3)
            if act.type.type == 'hiking': #Allot to hiking pool
                hikesum += float(re.sub(' SBD','',rewards))
                stats.pay_distance_hike += round(act.distance/1000,3)
                stats.save()
                total_hike_distance += round(act.distance/1000,3)
            if act.type.type == 'yoga': #allot to yoga pool
                yogasum += float(re.sub(' SBD', '',rewards))
                stats.pay_duration_yoga += round(act.duration/60,3)
                stats.save()
                total_yoga_duration += round(act.duration/60,3)
            benefsum+=float(re.sub(' SBD','',rewards))
    benefsum=benefsum*0.2 # 20% Beneficiary Share to exhaust
    benefsum=round(benefsum*0.5,3) # 50/50 Vest / SBD payout split -- 
    sum = (benefsum + blogsum + commentsum)
#    scalar = rewardbalance / sum
    runsum = round(runsum*0.1,3) # Total amount inrunning pool (SBD)
    bikesum = round(bikesum*0.1,3) #TOtal amount in cycling pool (SBD)
    hikesum = round(hikesum*0.1,3) #TOtal amount in hiking pool (SBD)
    yogasum = round(yogasum*0.1,3) #Total amount in yoga pool (SBD)
    print("Rewards found = " + str(sum)+" SBD")
    print("Rewards earned from cycling = " + str(bikesum) + " SBD")
    print("Rewards earned from running = " + str(runsum) + " SBD")
    print("Rewards earned from hiking = " + str(hikesum) + " SBD")
    print("Rewards earned from yoga = " + str(yogasum) + " SBD")
    print("Total rewards earned from activities = " + str(benefsum) + " SBD")
    print("Rewards earned from @exhaust comments = " + str(commentsum) + " SBD")
    print("Rewards earned from @exhaust posts = " + str(blogsum) + " SBD")
    reward_extra = round(blogsum+commentsum,3)
    print("Additional random rewards earned = " + str(reward_extra) + " SBD!")
    userList = User.objects.all()
    paysum = 0.0
    extra_each = reward_extra/len(payList)
    extra_each = round(extra_each,3)
    for user in userList: #Calculate payout for each user for each activity pool
        if user.username != "exhaustAdmin":
            stats = LeaderBoard.objects.get(user=user)
            rewardbike = 0.0
            rewardhike = 0.0
            rewardrun = 0.0
            rewardyoga = 0.0
            message2 = ''
            message3 = ''
            message4 = ''
            message5 = ''
            message6 = ''
            message7 = ''
            if extra_each > 0:
                message7=" There were even some rewards from donations or other beneficiary posts (to the tune of "+str(reward_extra)+" STEEM)! This means each of the "+str(len(payList))+" active users got an additional " + str(extra_each) + " SBD for using the system! "
            if stats.pay_distance_run > 0:
                type = activityType.objects.get(type='running')
                rewardrun = round(stats.pay_distance_run/total_run_distance*runsum*price,3) #user activity distance divided by total activity distance multiplied by reward pool(SBD) for activity
                rewardrun = round(rewardrun,3)
                total, events = check_tax(user,type)
                event_percent = 0.0
                all_events = 0.0
                message3 = str(round(rewardrun,3)) + " SBD for running approximately " + str(round(stats.pay_distance_run,3)) + "kms"
                if len(events)>=1:
                    for event in events:
                        event_percent = event.event_tax/total
                        all_events += event.event_tax/total
                        result = Result.objects.get(user=user,event=event)
#                        print(event_percent, rewardrun)
                        result.contribution_steem += event_percent*rewardrun
                        result.save()
                    rewardrun = rewardrun * (1-all_events)
                    message3 += "-- a portion of these rewards (" + str(all_events*100) + "%) has been held back to fund the running events you have signed up for"
            if stats.pay_distance_bike > 0:
                rewardbike = round(stats.pay_distance_bike/total_bike_distance*bikesum*price,3) #user activity distance divided by total activity distance multiplied by reward pool(SBD) for activity
                rewardbike = round(rewardbike,3)
                message2 =  "; " + str(round(rewardbike,3)) + " SBD for cycling approximately " + str(round(stats.pay_distance_bike,3)) + "kms, "
            if stats.pay_distance_hike > 0:
                rewardhike = round(stats.pay_distance_hike/total_hike_distance*hikesum*price,3) #user activity distance divided by total activity distance multiplied by reward pool(SBD) for activity
                rewardhike = round(rewardhike,3)
                message4 = str(round(rewardhike,3)) + " SBD for hiking approximately " + str(round(stats.pay_distance_hike,3)) + "kms! "
            if stats.pay_duration_yoga > 0:
                rewardyoga = round(stats.pay_duration_yoga/total_yoga_duration*yogasum*price,3) #user yoga duration divied by total yoga duration multiplied by reward pool (SBD) for yoga
                rewardyoga = round(rewardyoga,3)
                message5 = str(round(rewardyoga,3)) + " SBD from practicing yoga for approximately " + str(stats.pay_duration_yoga) + "minutes! "
            reward = round(rewardrun + rewardbike + rewardhike + rewardyoga,3)
            reward = round(reward,3)
            if user.username in payList:
                reward+=round(extra_each,3)
            paysum += reward
            paysum = round(paysum,3)
            message = "You have earned " + str(round(reward,3)) + " SBD for getting EXHAUSTED last week! "
            message6 = "Keep it up!"
            memo = message+message2+message3+message4+message5+message7+message6
            if reward>0:
                print(user.username, memo)
#                print(round(reward,3))
                s.transfer(user.username, amount = reward, asset='SBD', memo=memo, account='exhaust')
                time.sleep(3)
#        print(paysum)

def update_leaderboard():

    actList = Activity.objects.filter(posted="True")

    type = activityType.objects.get(type='running')
    runList = actList.filter(type=type)
    runList = list(reversed(runList))

    type = activityType.objects.get(type='cycling')
    cycleList = actList.filter(type=type)
    cycleList = list(reversed(cycleList))

    type = activityType.objects.get(type='hiking')
    hikeList = actList.filter(type=type)
    hikeList = list(reversed(hikeList))

    type = activityType.objects.get(type='yoga')
    yogaList = actList.filter(type=type)
    yogaList = list(reversed(yogaList))

    userList = User.objects.all()

    statlist = []
    statList = LeaderBoard.objects.all()

    now = datetime.now().timestamp()
    oneday = 24*60*60
    sevenday = 7*24*60*60
    thirtyday = 30*24*60*60

    for username in statList:
        statlist.append(str(username.user))
    for user in userList:
        if user.username not in statlist:
            stat = LeaderBoard(user = user)
            stat.save()
        else:
            stat = LeaderBoard.objects.get(user=user)
            stat.activitycount7 = 0
            stat.activitycount30 = 0
            stat.runcount7 = 0
            stat.bikecount7 = 0
            stat.hikecount7 = 0
            stat.yogacount7 = 0
            stat.rundistance7 = 0.0
            stat.rundistance30 = 0.0
            stat.runduration7 = 0.0
            stat.runduration30 = 0.0
            stat.runpace7 = 0.0
            stat.runpace30 = 0.0
            stat.runearned7 = 0.0
            stat.bikecount7 = 0.0
            stat.bikedistance7 = 0.0
            stat.bikedistance30 = 0.0
            stat.bikeduration7 = 0.0
            stat.bikeduration30 = 0.0
            stat.bikespeed7 = 0.0
            stat.bikespeed30 = 0.0
            stat.bikeearned7 = 0.0
            stat.hikecount7 = 0.0
            stat.hikedistance7 = 0.0
            stat.hikeduration7 = 0.0
            stat.hikeearned7 = 0.0
            stat.hikeclimb7 = 0.0
            stat.yogacount7 = 0.0
            stat.yogaduration7 = 0.0
            stat.yogaearned7 = 0.0
            stat.save()
    for run in runList:
        runtime = (run.postdate).timestamp()
        stat = LeaderBoard.objects.get(user=run.user)
        if int(now - runtime) <= sevenday:
            stat.activitycount7 += 1
            stat.runcount7 += 1
            stat.rundistance7 += round(run.distance/1000,2)
            stat.runduration7 += round(run.duration/60,2)
            stat.runpace7 = round((stat.runduration7)/(stat.rundistance7),2)
            stat.runearned7 += run.rewards
            stat.save()
#            print(run)
#            print("Updated a stat!")
        if int(now - runtime) <= thirtyday:
            stat.activitycount30 += 1
            stat.rundistance30 += run.distance/1000
            stat.runduration30 += run.duration/60
            stat.runpace30 = (stat.runduration30)/(stat.rundistance30)
            stat.save()
    for cycle in cycleList:
        cycletime = (cycle.postdate).timestamp()
        stat = LeaderBoard.objects.get(user=cycle.user)
        if int(now - cycletime) <= sevenday:
            stat.activitycount7 += 1
            stat.bikecount7 += 1
            stat.bikedistance7 += round(cycle.distance/1000,2)
            stat.bikeduration7 += round(cycle.duration/60,2)
            stat.bikespeed7 = round((stat.bikedistance7*60)/(stat.bikeduration7),2)
            stat.bikeearned7 += cycle.rewards
            stat.save()
 #           print(cycle)
 #           print("Updated a stat!")
    for hike in hikeList:
        hiketime = (hike.postdate).timestamp()
        stat = LeaderBoard.objects.get(user=hike.user)
        if int(now - hiketime) <= sevenday:
            stat.activitycount7 += 1
            stat.hikecount7 += 1
            stat.hikedistance7 += round(hike.distance/1000,2)
            stat.hikeduration7 += round(hike.duration/60,2)
            stat.hikeclimb7 += hike.ascent
            stat.hikeearned7 += hike.rewards
            stat.save()
            print(hike)
            print("Updated a stat!")
    for yoga in yogaList:
        yogatime = (yoga.postdate).timestamp()
        stat=LeaderBoard.objects.get(user=yoga.user)
        if int(now-yogatime) <= sevenday:
            stat.activitycount7 += 1
            stat.yogacount7 += 1
            stat.yogaduration7 += round(yoga.duration/60,2)
            stat.yogaearned7 += yoga.rewards
            stat.save()
            print(yoga)
            print("Updated a stat!")

def update_event_prizes():
    events = Event.objects.all()
    for event in events:
        print(event.name + ": " + str(event.prizepool))
        event.prizepool = 0.0
        print(event.name + ": " + str(event.prizepool))
        event.save()
    results = Result.objects.filter(final=False)
    for result in results:
        print(result.user.username + "'s contribution")
        event = Event.objects.get(pk = result.event.pk)
        print(event.name + ": " + str(event.prizepool))
        event.prizepool += result.contribution
        print(event.name + ": " + str(event.prizepool))
        print("==========================================")
        event.save()
