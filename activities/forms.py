from django import forms
from django.contrib.auth.decorators import login_required
from markdownx.fields import MarkdownxFormField

from .models import Event
#from world.models import WorldBorder

import floppyforms as floppy
import floppyforms.__future__ as flippy

#class RunForm(forms.ModelForm):
#    class Meta:
#        model = Run
#        fields = ('title', 'gpsfile', 'photo', 'distance', 'duration', 'comments')

class EventForm(flippy.ModelForm):
    class Meta:
        model = Event
        fields = ('name', 'event_type', 'event_date', 'post_body','post_tags')

class DatePicker(floppy.DateInput):  ##DATE WIDGET
    template_name = 'datepicker.html'

    class Media:
        js = (
            'js/jquery.min.js',
            'js/jquery-ui.min.js',
        )
        css = {
            'all': (
                'css/jquery-ui.css',
            )
        }

class TagForm(floppy.Form):
    tags = forms.CharField(min_length=3, max_length=300, required=True)

class TimePicker(forms.TimeInput):
    template_name = 'time.html'
    format='%H:%M:%S'


class Textarea(floppy.Textarea):
    template_name = 'textarea.html'
    rows = 5
    cols = 30


class Select(floppy.Select):
    template_name = 'select.html'


class DataForm(floppy.Form):
    title = forms.CharField(min_length=6, max_length=50, required=True)
    distance = forms.FloatField(min_value = 0.1, max_value=60.0, required=False)
    duration = floppy.CharField(max_length=10,required=False)
    time = floppy.TimeField(widget=floppy.TimeInput(format='%H:%M:%S'),required=False)
    date = forms.DateField(widget=DatePicker,required=False)

class YogaForm(floppy.Form):
    title = floppy.CharField(max_length=120, required=True)
    video_URL = floppy.CharField(min_length=6, max_length=140, required=False)
    duration = floppy.CharField(max_length=10,required=True)
    style = floppy.CharField(max_length=20, required=False)
    instructor = floppy.CharField(max_length=50, required=True)

class StrengthForm(floppy.Form):
    title = floppy.CharField(max_length=120, required=True)
    duration = floppy.CharField(max_length=10, required=True)
    equipment_used = floppy.CharField(max_length=120, required=True)
    muscle_group = floppy.CharField(required=True)

class PhotoForm(floppy.Form):
    photo1 = forms.ImageField(allow_empty_file=True, required=False)
    photo2 = forms.ImageField(allow_empty_file=True, required=False)
    photo3 = forms.ImageField(allow_empty_file=True, required=False)

class GPSForm(floppy.Form):
    gpsfile = forms.FileField(allow_empty_file=True, required=False)

class ProfileForm(forms.ModelForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

class CommentForm(floppy.Form):
    description = floppy.CharField(widget=Textarea)

class TagForm(floppy.Form):
    tags = floppy.CharField(min_length=10, required=True, max_length=128)

class ReplyForm(forms.Form):
    reply = forms.CharField(min_length=1, max_length=140, required=True)

class Slider(floppy.RangeInput):  ##SLIDER WIDGET
    min = 5
    max = 100
    step = 5
    template_name = 'slider.html'

    class Media:
        js = (
            'activities/assets/js/jquery.min.js',
            'activities/assets/js/jquery-ui.min.js',
        )
        css = {
            'all': (
                'activities/assets/css/jquery-ui.css',
            )
        }

class SlideForm(floppy.Form):
    num = floppy.IntegerField(widget=Slider)
    min = 5
    max = 100
    step = 5

    def clean_num(self):
        num = self.cleaned_data['num']
        if not 5 <= num <= 100:
            raise forms.ValidationError("Enter a value between 5 and 20")

        if not num % 1 == 0:
            raise forms.ValidationError("Enter a multiple of 5")
        return num

class MarkdownForm(forms.Form):
    Markdown_Editor = MarkdownxFormField()

class OsmPolyWidget(flippy.gis.MultiPolygonWidget, flippy.gis.BaseOsmWidget):
    pass

#class GeoForm(flippy.ModelForm):
#    point = flippy.gis.MultiPolygonField(widget=OsmPolyWidget(attrs={
#        'map_width': 480,
#        'map_height': 480,
#    }))
#    class Meta:
#        model = WorldBorder
#        fields = {'name','area'}
