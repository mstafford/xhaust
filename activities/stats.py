from activities.models import Action, activityType, LeaderBoard
from datetime import datetime
from django.contrib.auth.models import User

def update_activity_stats():
    typeList = ["running","cycling","hiking","yoga"]
    userList = User.objects.all().exclude(username="exhaustAdmin")
    now = datetime.now()
    month = now.month
    weekday = now.weekday()
    year = now.year
    day = now.day
    now = now.timestamp()
    lastseven = [now - 3600*24*7, "Last Seven Days"]
    lastthirty = [now - 3600*24*30, "Last Thirty Days"]
    lastninety = [now - 3600*24*90, "Last Ninety Days"]
    lastyear = [now - 3600*24*365, "Last 365 Days"]
    alltime = [0,"All Time"]
    thisweek = [datetime(year,month,day-weekday).timestamp(),"This Week"]
    thismonth = [datetime(year,month,1).timestamp(),"This Month"]
    thisyear = [datetime(year,1,1).timestamp(),"This Year"]
    timeranges = [lastseven, lastthirty, lastninety, lastyear, alltime, thisweek, thismonth, thisyear]
    rangedistance = 0.0
    rangeduration = 0.0
    rangecount = 0
    actcounter = 0
    actearned = 0.0
    for user in userList: ## Update stats for each user
        for acttype in typeList:  ## Update stats for each activity type
            actType = activityType.objects.get(type=acttype) 
            actList = Action.objects.filter(user=user, type=actType, posted=True) ## Pull list of each users activities of certain type
            userstats = LeaderBoard.objects.get(user=user)
            for range in timeranges:
                print("Tabulating @"+str(user.username)+"'s " + str(actType.type) + " activities for " + range[1])
                rangedistance = 0.0
                rangeduration = 0.0
                rangecount = 0
                actcounter = 0
                actearned = 0.0
                acts = actList.filter(postdate__gte=datetime.fromtimestamp(range[0]))
                for act in acts:
                    rangedistance += act.distance/1000
                    rangeduration += act.duration/60
                    rangecount += 1
                    actcounter += 1
                    actearned += act.rewards
                if rangedistance > 0:
                    print("Distance: " + str(round(rangedistance,2)) + "km " + str(actType.type) + range[1])
                if rangeduration > 0:
                    print("Duration: " + str(round(rangeduration,2)) + " minutes " + range[1])
                if range[1] == "This Week":
                    if actType.type == "running":
                        userstats.runearnedWTD = actearned
                        userstats.runcountWTD = actcounter
                        userstats.rundistanceWTD = rangedistance
                        userstats.rundurationWTD = rangeduration
                    if actType.type == "cycling":
                        userstats.bikeearnedWTD = actearned
                        userstats.bikecountWTD = actcounter
                        userstats.bikedistanceWTD = rangedistance
                        userstats.bikedurationWTD = rangeduration
                    if actType.type == "hiking":
                        userstats.hikeearnedWTD = actearned
                        userstats.hikecountWTD =actcounter
                        userstats.hikedistanceWTD = rangedistance
                        userstats.hikedurationWTD = rangeduration
                    if actType.type == "yoga":
                        userstats.yogaearnedWTD = actearned
                        userstats.yogacountWTD = actcounter
                        userstats.yogadurationWTD = rangeduration
                if range[1] == "This Month":
                    if actType.type == "running":
                        userstats.runearnedMTD = actearned
                        userstats.runcountMTD = actcounter
                        userstats.rundistanceMTD = rangedistance
                        userstats.rundurationMTD = rangeduration
                    if actType.type == "cycling":
                        userstats.bikeearnedMTD = actearned
                        userstats.bikecountMTD = actcounter
                        userstats.bikedistanceMTD = rangedistance
                        userstats.bikedurationMTD = rangeduration
                    if actType.type == "hiking":
                        userstats.hikeearnedMTD = actearned
                        userstats.hikecountMTD = actcounter
                        userstats.hikedistanceMTD = rangedistance
                        userstats.hikedurationMTD = rangeduration
                    if actType.type == "yoga":
                        userstats.yogaearnedMTD = actearned
                        userstats.yogacountMTD = actcounter
                        userstats.yogadurationMTD = rangeduration
                if range[1] == "This Year":
                    if actType.type == "running":
                        userstats.runearnedYTD = actearned
                        userstats.runcountYTD = actcounter
                        userstats.rundistanceYTD = rangedistance
                        userstats.rundurationYTD = rangeduration
                    if actType.type == "cycling":
                        userstats.bikeearnedYTD = actearned
                        userstats.bikecountYTD = actcounter
                        userstats.bikedistanceYTD = rangedistance
                        userstats.bikedurationYTD = rangeduration
                    if actType.type == "hiking":
                        userstats.hikeearnedYTD = actearned
                        userstats.hikecountYTD = actcounter
                        userstats.hikedistanceYTD = rangedistance
                        userstats.hikedurationYTD = rangeduration
                    if actType.type == "yoga":
                        userstats.yogaearnedYTD = actearned
                        userstats.yogacountYTD = actcounter
                        userstats.yogadurationYTD = rangeduration
                userstats.save()
#            print(userstats.rundistance7, userstats.runduration7)


