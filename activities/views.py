from activities.gpxParse import importGPX, convertUTM, runLength, runTime, runPace
from activities.commenter import sport_summary
from activities.models import LeaderBoard, activityType, xhstSettings, Event, Result
from activities.models import Action as Activity
from activities.plot import paceChart, paceChart2, cadenceChart, heartChart, elevChart, speedChart, splitChart, wattageChart, volumeSeven, volumeThirty, volumeNinety, trainingvols
from activities.workout_types import STR_WORKOUT_CHOICES, CLIMBING_CHOICES, CLIMBING_GRADES, BOULDERING_GRADES 
#from world.models import WorldBorder
from steemvoter.models import Commenter, VoteQueue

from . import charts

from beem.account import Account
from beem.comment import Comment as Bomment
from beem.steem import Steem as Beem
from bs4 import BeautifulSoup
from datetime import datetime
from django_ajax.decorators import ajax
from django_ajax.mixin import AJAXMixin
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView
from .activity_sorter import feedAll
from .forms import SlideForm, CommentForm, DataForm, PhotoForm, DatePicker, ReplyForm, GPSForm, MarkdownForm, Slider, TagForm, EventForm, StrengthForm, YogaForm
from .gpxParse import importGPX, convertUTM, runLength, runTime, runPace, importTCX
from .img_scale import downscale
from jchart import Chart
from markdown2 import Markdown
#from social_django.models import UserSocialAuth
from stat import *
from steem import Steem
from steemconnect.operations import Vote, Comment, CommentOptions
from steemconnect.client import Client
from steemconnect.steemconnect import SteemConnect
from steemconnect_auth.models import SteemConnectUser
from steem.converter import Converter
from steem.post import Post

import gpxpy, gpxpy.gpx
import humanize
import json
import magic
import markdown2
import math
import os
import re
import requests
import tcxparser
import time

from .secret import *

# Create your views here.

class SignUpView(CreateView):
    template_name = 'signup.html'
    form_class = UserCreationForm

def map_privacy(request, target, level):
    user = User.objects.get(username=request.user)
    settings = 'null'
    try:
        settings = xhstSettings.objects.get(user=user)
    except:
        settings = xhstSettings(user=user)
        settings.save()
    if target == 'default':
        settings.showmaps = level
        settings.save()
    else:
        actID = int(target)
        action = Activity.objects.get(pk=actID)
        action.showmap = level
        action.save()
    return redirect('/dash/')

def delete_activity(request, activity):
    #Check to make sure activity is owned by user
    user = User.objects.get(username=request.user)
    chop = Activity.objects.get(pk=activity)
    if user.username == chop.user.username:
        chop.delete()
        return redirect('/dash/')
    else:
        return redirect('/')

def votecalc(request):
    if request.method == 'POST':
        weight = request.POST.get('num')
        response_data = {}
        account = Account(str(request.user))
        response_data['weightvalue']=round(account.get_voting_value_SBD(int(weight)),3)
        response_data['weight']=str(weight)

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def calc_votevalue():
#    weight = request.GET.get('weight', None)
    data = {
        'weight': 30,
        'username': 'mstafford',
    }
    return JsonResponse(data)

def index(request):
    template = loader.get_template('activities/index.html', using='backend')
    good_reads = {}
    blog_posts = {}
    s = Steem()
#    blogroll = s.get_blog('exhaust',-1,10)             #fixing shit for mark --john
    blogroll = s.get_blog('exhaust',0,50)
    readcounter = 0
    blogcounter = 0
    userList = User.objects.all()
    if len(userList) > 0:
        activityleader = ['null',0] #[username, count, post-title, post-link]
        distanceleader = ['null',0] #[username, distance, post-title, post-link]
        paceleader = ['null',100] #[username, pace, post-title, post-link]
        ## Activity Counter
        if len(LeaderBoard.objects.all()) > 0:
            countleader = list(reversed(LeaderBoard.objects.order_by('activitycount7')))[:1]
            user = countleader[0].user
            activity = list(Activity.objects.filter(user=User.objects.get(username=user)))[-1]
            activityleader = [user.username, countleader[0].activitycount7, activity.title, activity.id]

            distleader = list(reversed(LeaderBoard.objects.order_by('rundistance7')))[:1]
            try:
                user = distleader[0].user
                activity = list(Activity.objects.filter(user=user))[-1]
                distanceleader = [user.username, round(distleader[0].rundistance7,2), activity.title, activity.id]
            except:
                distanceleader=['SpaceGhost','69', 'SpaceGhosting Coast2COast', '69']

            speedleader = LeaderBoard.objects.order_by('runpace7')
            speedleader = list(speedleader.exclude(runpace7=0))
            try:
                user = speedleader[0].user
                activity = list(Activity.objects.filter(user=user))[-1]
                paceleader = [user.username, round(speedleader[0].runpace7,2), activity.title, activity.id]
            except:
                paceleader=['SpaceGhost','69','SpaceGhosting Coast2Cosst','69']
    else:
        activityleader = ['SpaceGhost','69','SpaceGhosting Coast2Coast','69']
        distanceleader = ['SpaceGhost','69','SpaceGhosting Coast2Coast','69']
        paceleader = ['SpaceGhost','69','SpaceGhosting Coast2Coast','69']

    for post in blogroll:
        comment = post['comment']
        author = comment['author']
        if comment['json_metadata']:
            json_metadata = json.loads(comment['json_metadata'])
        if author == 'exhaust': ## Populate blog posts at bottom of page with articles written by @RB
            img_link = re.search('!\[.{0,30}]\(.{10,200}\)',comment['body'])
            if img_link:
                img_link = re.sub('!\[.{0,30}]\(','',img_link[0])
                img_link = re.sub('\)','',img_link)
            blog_posts[blogcounter]={}
            blog_posts[blogcounter]['created']=humanize.naturaldelta(datetime.strptime(comment['created'],'%Y-%m-%dT%H:%M:%S'))
            blog_posts[blogcounter]['title']=comment['title']
            blog_posts[blogcounter]['preview']=comment['body'][:280]
            blog_posts[blogcounter]['author']='exhaust'
            blog_posts[blogcounter]['body']=comment['body']
            blog_posts[blogcounter]['imglink']=img_link
            blog_posts[blogcounter]['permlink']=comment['permlink']
            blogcounter += 1
        if author != 'exhaust' and author != '':
            img_link = re.search('!\[.{0,256}]\(.{10,256}\)',comment['body'])
            if img_link:
                img_link = re.sub('!\[.{0,256}]\(','',img_link[0])
                img_link = re.sub('\)','',img_link)
                img_link = re.sub('https://steemitimages.com/\d{1,3}x\d{1,3}/','',img_link)
                if 'steepshot' in json_metadata['app']: ## Ensure that steepshot posts can be resteemed nicely
                    img_link = json_metadata['image']
                    img_link = img_link[0]
            else:
                img_link = ''
            if 'video' in json_metadata: ## Ensure that dTube posts cane be resteemed nicely
#                dtubedata = json.loads(json_metadata)
                try:
                    img_link = json_metadata['image'][0]
                except:
                    img_link = 'https://ipfs.io/ipfs/'+json_metadata['video']['info']['snaphash']
            good_reads[readcounter]={} ## Populate front-page articles w/ resteemed blog posts
            good_reads[readcounter]['created']=humanize.naturaldelta(datetime.strptime(comment['created'],'%Y-%m-%dT%H:%M:%S'))
            good_reads[readcounter]['title']=comment['title']
            good_reads[readcounter]['preview']=comment['body'][:140]
            good_reads[readcounter]['body']=comment['body']
            good_reads[readcounter]['author']=author
            good_reads[readcounter]['imglink']=img_link
            good_reads[readcounter]['permlink']=comment['permlink']
            readcounter +=1
            if readcounter >= 20:
                break
    if str(request.user) != 'AnonymousUser':
        user = (User.objects.get(username=request.user))
        context = {
            'STEEM_ID': user.username,
            'GOOD_READS': good_reads,
            'BLOG': blog_posts,
            'ACTIVITYLEADER': activityleader,
            'DISTANCELEADER': distanceleader,
            'PACELEADER': paceleader,
        }
    else:
        context = {
            'GOOD_READS': good_reads,
            'BLOG': blog_posts,
            'ACTIVITYLEADER': activityleader,
            'DISTANCELEADER': distanceleader,
            'PACELEADER': paceleader,
        }
    return HttpResponse(template.render(context,request))

def ajaxtest(request):
    c = 2 + 3
    return HttpResponse("Hello")

def test_feat(request):
    template = loader.get_template('dynoform.html', using='backend')
    photoform = PhotoForm()
    gpsform = GPSForm()
    context = {
        'photoform':photoform,
        'gpsform': gpsform,
    }
    return HttpResponse(template.render(context,request))

def new_event(request):
    template = loader.get_template('activities/post_editor.html', using='backend')
    user = User.objects.get(username=str(request.user))
    post = Post('@mstafford/a-dozen-days-and-a-lucky-13th-session')
    content = post.body
    if str(request.user) != 'exhaust' and str(request.user) != 'mstafford':
        return HttpResponse('You do not have permissions to create an event yet.')
    if request.method == 'POST':
        eventform = EventForm(request.POST)
        eventform.event_author = user
        eventform.save()
        event = Event.objects.last()
        return redirect('/event/' + str(event.pk))
    eventform = EventForm()
    markdownform = MarkdownForm(initial={'Markdown_Editor':content})
    context = {
    'eventform': eventform,
    'markdownform': markdownform
    }
    return HttpResponse(template.render(context,request))

def join_or_leave_event(request, event_id):
    user = User.objects.get(username=str(request.user))
    event = Event.objects.get(pk=event_id)
    if user in event.participants.all():
        event.participants.remove(user)
        result = Result.objects.get(user=user, event=event)
        result.active = False
        result.save()
    else:
        event.participants.add(user)
        try:
            result = Result.objects.get(user=user, event=event)
            result.active = True
            result.save()
        except:
            result = Result(user=user, event=event)
            result.save()
    return redirect('/events/all/')

def testweight(request, weight):
    return HttpResponse("Vote weight = " + str(weight) + "%")

def article(request, steem_id, link):
    post = Post('@'+steem_id+'/'+link)
    voterList = []
    voted = False
    user = 'null'
    plink = '@'+steem_id+'/'+link
    try:
        user = User.objects.get(username=request.user)
    except:
        user = 'null'
    for voter in post['active_votes']:
        voterList.append(voter['voter'])
        if str(user) in voterList:
            voted = True
    replies = list(Post.get_replies(post))
    if len(replies)>=1:
        comments = getReplies(replies, user)
    else:
        comments = []
    markdowner = Markdown()
    pattern_jpg = '(?<!\()https://\S+.jpg'
    pattern_png = "(?<!\=\')https://\S+.png"
    pattern_gif = '(?<!\()https://\S+.gif'

    jpglinks = re.findall(pattern_jpg,post.body)
#    for jpglink in jpglinks:
#        post.body = re.sub(jpglink,"![image]("+jpglink+")",post.body)

#    pnglinks = re.findall(pattern_png,post.body)
#    for pnglink in pnglinks:
#        post.body = re.sub(pnglink,"![image]("+pnglink+")",post.body)

    giflinks = re.findall(pattern_gif,post.body)
    for giflink in giflinks:
        post.body = re.sub(giflink,"![image]("+giflink+")",post.body)

#    postBody = markdowner.convert(post.body)
    postBody = markdown2.markdown(post.body,extras=['tables'])
    soup = BeautifulSoup(postBody, 'html.parser')
    images = soup.find_all('img')
    for image in images:
        image['class']='responsive'
    postBody = soup
    template = loader.get_template('activities/post_full.html', using='backend')
    if request.method == 'POST':
        commenter = CommentForm(request.POST)
        if commenter.is_valid():
            body = commenter.cleaned_data['description']
            createComment(request, str(post.author),str(plink), str(body))
    else:
        commenter = CommentForm()
    beem = Beem()
    if str(request.user) != "AnonymousUser":
        account = Account(str(request.user))
        vests = round(beem.vests_to_sp(account.get_vests()),3)
        votepower = str(round(account.get_voting_power(),2))+"%"
        voteval = str(round(account.get_voting_value_SBD(),3))
    else:
        account="null"
        vests = 100
        votepower = 100
        voteval = 0
    if request.method == 'POST':
        votewt = SlideForm(request.POST)
    else:
        votewt = SlideForm()

    context = {
        'AUTHOR':post.author,
        'BODY':postBody,
        'TITLE':post.title,
        'PERMLINK': link,
        'COMMENTS':comments,
        'COMMENTER':commenter,
        'VESTS':vests,
        'VOTINGPOWER':votepower,
        'FULLVALUE':voteval,
        "SLIDERFORM":votewt,
        'VOTED':voted,
    }
    return HttpResponse(template.render(context,request))

def leaderboard(request):
    s = Steem()
    template = loader.get_template('activities/leaderboard.html', using='backend')
    leaderboards =LeaderBoard.objects.all()
    runLeaders = leaderboards.order_by('rundistanceWTD').reverse().exclude(rundistanceWTD=0)
    cycleLeaders = leaderboards.order_by('bikedistanceWTD').reverse().exclude(bikedistanceWTD=0)
    hikeLeaders = leaderboards.order_by('hikedistanceWTD').reverse().exclude(hikedistanceWTD=0)
    yogaLeaders = leaderboards.order_by('yogadurationWTD').reverse().exclude(yogacountWTD=0)

    context = {}
    RUNNERS = {}
    CYCLISTS = {}
    HIKERS = {}
    YOGIS = {}
    runReward = 0.0
    runPayout = 0.0
    runKMs = 0.0
    cycleReward = 0.0
    cyclePayout = 0.0
    cycleKMs = 0.0
    hikeReward = 0.0
    hikePayout = 0.0
    hikeKMs = 0.0
    yogaReward = 0.0
    yogaPayout = 0.0
    yogaTime = 0.0
    for leader in runLeaders:
        ## RUNNERS['runner'] = [NAME, n, DISTANCE, DURATION, REWARDS]
        RUNNERS[str(leader.user)]=[str(leader.user),str(leader.runcountWTD),str(round(leader.rundistanceWTD,2)),str(round(leader.rundurationWTD,2)),str(round(leader.runearnedWTD/10,3))]
        runKMs += leader.rundistanceWTD
        runReward += leader.runearnedWTD/10
    for runner in RUNNERS:
        RUNNERS[runner].append(str(round((float(RUNNERS[runner][2])/runKMs)*runReward,3)))
        runPayout += float(RUNNERS[runner][5])
    RUNNERS['SUM'] = ['~~~SUM~~~','---',round(runKMs,2),'---','---',round(runReward,3),round(runPayout,3)]
    #Repeat for other activities

    for leader in cycleLeaders:
        ## CYCLISTS['cyclist'] = [NAME, n, DISTANCE, DURATION, REWARDS]
        CYCLISTS[str(leader.user)]=[str(leader.user),leader.bikecountWTD,round(leader.bikedistanceWTD,2),round(leader.bikedurationWTD,2),round(leader.bikeearnedWTD/10,3)]
        cycleKMs += leader.bikedistanceWTD
        cycleReward += leader.bikeearnedWTD/10
    for cyclist in CYCLISTS: 
        CYCLISTS[cyclist].append(str(round((float(CYCLISTS[cyclist][2])/cycleKMs)*cycleReward,3)))
        cyclePayout += float(CYCLISTS[cyclist][5])
    CYCLISTS['SUM'] = ['~~~SUM~~~','---',str(round(cycleKMs,2)),'---','---',round(cycleReward,3),round(cyclePayout,3)]

    for leader in hikeLeaders:
        ## HIKERS['hiker'] = [NAME, n, distance, duration, rewards]
        HIKERS[str(leader.user)]=[str(leader.user),leader.hikecountWTD,round(leader.hikedistanceWTD,2),round(leader.hikedurationWTD,2),round(leader.hikeearnedWTD/10,3)]
        hikeKMs += leader.hikedistanceWTD
        hikeReward += leader.hikeearnedWTD/10
    for hiker in HIKERS:
        HIKERS[hiker].append(str(round((float(HIKERS[hiker][2])/hikeKMs)*hikeReward,3)))
        hikePayout += float(HIKERS[hiker][5])
    HIKERS['SUM'] = ['~~~SUM~~~','---',str(round(hikeKMs,2)),'---','---',round(hikeReward,3),round(hikePayout,3)]

    for leader in yogaLeaders:
        YOGIS[str(leader.user)]=[str(leader.user),leader.yogacountWTD,round(leader.yogadurationWTD,2),round(leader.yogaearnedWTD/10,3)]
        yogaTime += leader.yogadurationWTD
        yogaReward += leader.yogaearnedWTD/10
    for yoga in YOGIS:
        try:
            YOGIS[yoga].append(str(round((float(YOGIS[yoga][2])/yogaTime)*yogaReward,3)))
        except:
            YOGIS[yoga].append(str(0))
        yogaPayout += float(YOGIS[yoga][3])
    YOGIS['SUM'] = ['~~~SUM~~~','---',str(round(yogaTime,2)),'---','---',round(yogaReward,3),round(yogaPayout,3)]


    totalReward = runReward + cycleReward + hikeReward
    context = {
        'RUNNERS':RUNNERS,
        'RUNREWARD':runReward,
        'CYCLISTS':CYCLISTS,
        'CYCLEREWARD':cycleReward,
        'HIKERS':HIKERS,
        'YOGIS':YOGIS,
        'YOGAREWARD': yogaReward,
        'HIKEREWARD':hikeReward,
    }
    return HttpResponse(template.render(context,request))

def update_dash(request):

    actDist = 0
    actTime = 0
    actReward = 0
    if request.method == 'POST':
        user = User.objects.get(username=request.user)
        actType = activityType.objects.get(type=request.POST['activity'])
        actList = Activity.objects.filter(user=user, type=actType)
        if actType.type == "yoga":
            for act in actList:
                actTime += act.duration
                actReward += act.rewards
        else:
            for act in actList:
                actDist += act.distance/1000
                actTime += act.duration
                actReward += act.rewards
        stats = {}
        stats['count'] = len(actList)
        stats['distance'] = round(actDist,2)
        stats['duration'] = str(int(actTime/3600))+"hr:"+str(int(actTime%3600/60))+"min:"+str(int(actTime%60))+"sec"
        stats['reward'] = round(actReward,3)
        response_data = {}
        response_data['volseven'] = volumeSeven(user,actType)
        response_data['volthirty'] = volumeThirty(user,actType)
        response_data['volninety'] = volumeNinety(user,actType)
        response_data['stats'] = stats
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )


def dash(request):
    s = Steem()
    c = Converter()
    template = loader.get_template('activities/dash.html', using='backend')
    user = User.objects.get(username=request.user)
    try:
        account_settings = xhstSettings.objects.get(user=user)
    except:
        account_settings = xhstSettings(user=user)
        account_settings.save()
    account_info = s.get_account(user.username)
    volsevenHTML = ''
    volthirtyHTML = ''
    volninetyHTML = ''
    followerList = []
    followingList = []
    friendList = []
    followers = s.get_followers(user.username,0,'blog',1000)
    followings = s.get_following(user.username, 0, 'blog', 1000)
    for follow in followers:
        followerList.append(follow['follower'])
    for follow in followings:
        followingList.append(follow['following'])
    for follow in followingList:
        if follow in followerList:
            friendList.append(follow)
    if account_info['json_metadata'] != '{}':
        json_data = json.loads(account_info['json_metadata'])
    else:
        json_data = {}
        json_data['profile']={}
        json_data['profile']['about']='EXHAUSTed'
    ## Need to check if 'about' key exists
    if 'about' not in json_data['profile']:
        json_data['profile']['about'] = 'EXHAUSTed'
    if 'profile_image' not in json_data['profile']:
        json_data['profile']['profile_image'] = 'https://cdn3.volusion.com/qz29d.hncd6/v/vspfiles/photos/PUN-497-2.jpg?1487081183'
    ## Some activity information below:
    dbUser = User.objects.get(username = user.username)
    type = activityType.objects.get(type='running')
    activityList = Activity.objects.filter(user=dbUser)
    runList = activityList.filter(type=type)
    runList = list(reversed(runList))
    runHistory = []
    for run in runList:
        runHistory.append([run.pk, run.title, humanize.naturalday(run.uploaded_at), run.posted])
    type = activityType.objects.get(type='cycling')
    cycleList = activityList.filter(type=type)
    cycleList = list(reversed(cycleList))
    cycleHistory = []
    for cycle in cycleList:
        cycleHistory.append([cycle.pk, cycle.title, humanize.naturalday(cycle.uploaded_at), cycle.posted])

    type = activityType.objects.get(type='hiking')
    hikeList = activityList.filter(type=type)
    hikeList = list(reversed(hikeList))
    hikeHistory = []
    for hike in hikeList:
        hikeHistory.append([hike.pk, hike.title, humanize.naturalday(hike.uploaded_at), hike.posted])

    type = activityType.objects.get(type='yoga')
    yogaList = activityList.filter(type=type)
    yogaList = list(reversed(yogaList))
    yogaHistory = []
    for yoga in yogaList:
        yogaHistory.append([yoga.pk, yoga.title, humanize.naturalday(yoga.uploaded_at), yoga.posted])
    runCount = len(runList)
    cycleCount = len(cycleList)
    hikeCount = len(hikeList)
    yogaCount = len(yogaList)
    runDist = 0
    runTime = 0
    runReward = 0
    cycleDist = 0
    cycleTime = 0
    cycleReward = 0
    hikeDist = 0
    hikeTime = 0
    hikeReward = 0
    yogaTime = 0
    yogaReward = 0
    user = User.objects.get(username=request.user)
    actType = activityType.objects.get(type="running")
    volsevenHTML = volumeSeven(user,actType)
    volthirtyHTML = volumeThirty(user,actType)
    volninetyHTML = volumeNinety(user,actType)
#    volHTML = trainingvols(user,actType)
    for run in runList:
        runDist += run.distance
        runTime += run.duration
        runReward += run.rewards
    for cycle in cycleList:
        cycleDist += cycle.distance
        cycleTime += cycle.duration
        cycleReward += cycle.rewards
    for hike in hikeList:
        hikeDist += hike.distance
        hikeTime += hike.duration
        hikeReward += hike.rewards
    for yoga in yogaList:
        yogaTime += yoga.duration
        yogaReward += yoga.rewards
    steempower = round(c.vests_to_sp(float(re.sub('VESTS','',(account_info['vesting_shares'])))),3)
    context = {
        'STEEM_ID': user.username,
        'SBD_BAL': account_info['sbd_balance'],
        'STEEM_BAL': account_info['balance'],
        'STEEM_POW': str(steempower),
        'ABOUT': json_data['profile']['about'],
        'IMAGE': json_data['profile']['profile_image'],
        'MAP_PRIV': account_settings.showmaps,
        'VOLSEVEN': volsevenHTML,
        'VOLTHIRTY': volthirtyHTML,
        'VOLNINETY': volninetyHTML,
#        'VOLHTML': volHTML,
        'RUN_COUNT': runCount,
        'RUN_LIST': runList,
        'RUN_HISTORY': runHistory[:5],
        'CYCLE_COUNT': cycleCount,
        'CYCLE_LIST': cycleList,
        'CYCLE_HISTORY': cycleHistory[:5],
        'HIKE_COUNT': hikeCount,
        'HIKE_LIST': hikeList,
        'HIKE_HISTORY': hikeHistory[:5],
        'YOGA_COUNT': yogaCount,
        'YOGA_LIST': yogaList,
        'YOGA_HISTORY': yogaHistory[:5],
        'RUN_DIST': runDist/1000,
        'RUN_HRS': int(runTime/3600),
        'RUN_MINS': int(runTime%3600/60),
        'RUN_SECS': runTime%60,
        'RUN_REWARD': round(runReward,3),
        'CYCLE_DIST': cycleDist/1000,
        'CYCLE_HRS': int(cycleTime/3600),
        'CYCLE_MINS': int(cycleTime%3600/60),
        'CYCLE_SECS': cycleTime%60,
        'CYCLE_REWARD': round(cycleReward,3),
        'HIKE_DIST': hikeDist/1000,
        'HIKE_HRS': int(hikeTime/3600),
        'HIKE_MINS': int(hikeTime%3600/60),
        'HIKE_SECS': hikeTime%60,
        'HIKE_REWARD': round(hikeReward,3),
        'YOGA_HRS': int(yogaTime/3600),
        'YOGA_MINS': int(yogaTime%3600/60),
        'YOGA_SECS': yogaTime%60,
        'YOGA_REWARD': round(yogaReward,3),
        'FOLLOWERS': len(followerList),
        'FOLLOWING': len(followingList),
        'FRIENDS': len(friendList),
    }
    return HttpResponse(template.render(context,request))

def events(request, activity_type):
    template = loader.get_template('activities/joinevent.html',using='backend')
    try:
        user = User.objects.get(username=request.user)
    except:
        user = 'null'
    eventList = []
    if activity_type == 'all':
        events = Event.objects.filter(posted=True).order_by('event_date')
    else:
        if activity_type == 'running':
            type = activityType.objects.get(type='running')
        if activity_type == 'cycling':
            type = activityType.objects.get(type='cycling')
        if activity_type == 'hiking':
            type = activityType.objects.get(type='hiking')
        events = Event.objects.filter(event_type=type).order_by('event_date')
    context={
    'USER':user,
    'MODEL':events
    }
    return HttpResponse(template.render(context,request))

def feed(request, activity_type, sort):
    s = Steem()
    beem = Beem()
    try:
        user = User.objects.get(username=request.user)
#        followlist = s.get_following(str(user),0,'blog',1000)
#        friendList = []
#        for friend in followlist:
#            friendList.append(friend['following'])
    except:
        user = 'null'
    if activity_type == 'running':
        type = activityType.objects.get(type='running')
        activityList = Activity.objects.filter(type=type).filter(posted='True').order_by('postdate')
        activityList = activityList.reverse()
    if activity_type == 'cycling':
        type = activityType.objects.get(type='cycling')
        activityList = Activity.objects.filter(type=type).filter(posted='True').order_by('postdate')
        activityList = activityList.reverse()
    if activity_type == 'hiking':
        type = activityType.objects.get(type='hiking')
        activityList = Activity.objects.filter(type=type).filter(posted='True').order_by('postdate')
        activityList = activityList.reverse()
    if activity_type == 'yoga':
        type = activityType.objects.get(type='yoga')
        activityList = Activity.objects.filter(type=type).filter(posted='True').order_by('postdate')
        activityList = activityList.reverse()
    if activity_type == 'all':
        activityList = Activity.objects.all().filter(posted='True').order_by('postdate')
        activityList = activityList.reverse()
    activityList = activityList[:20]
    myFeed = {}
#    return HttpResponse("EXHAUST FEED IS CURRENTLY UNDER MAINTENANCE. WE'LL BE BACK SOON!")
    recent_votes = []
    if str(request.user) != 'AnonymousUser':
        account = Account(str(request.user))
        recent_votes = account.get_account_votes()
        vests = round(beem.vests_to_sp(account.get_vests()),3)
        votepower = str(round(account.get_voting_power(),2))+"%"
        voteval = str(round(account.get_voting_value_SBD(),3))
#        vests = 100
#        votepower = 100
#        voteval = 0
    else:
        vests = 100
        votepower = 100
        voteval = 0
    for activity in activityList:
        post = Post(activity.permlink)
        comment = Bomment(activity.permlink)
        myFeed[activity]=[]
        activity.reward = post.reward
        activity.duration = int(activity.duration/60)
        activity.distance = activity.distance/1000
        activity.brief = activity.comments[:300]+'...'
        activity.inpast = humanize.naturaldelta(post.created)
#        for vote in recent_votes:
#            permlink = re.sub('','@',activity.permlink)
#            if  vote['authorperm'] == permlink:
#                activity.voted = True
#                print("User voted on activity!")
#                break
#            else:
#                activity.voted = False
        votes = comment.get_votes()
        try:
            if str(user.username) in votes:
                activity.voted = True
            else:
                activity.voted = False
        except:
            activity.voted = False

#        for voter in post['active_votes']:
#            myFeed[activity].append(voter['voter'])
#            if str(user) in myFeed[activity]:
#                activity.voted = True

    template = loader.get_template('activities/feed.html', using='backend')
    if request.method == 'POST':
        votewt = SlideForm(request.POST)
    else:
        votewt = SlideForm()

    context = {
        "POSTLIST":activityList,
        "FEEDLIST":myFeed,
        'VESTS':vests,
        'VOTINGPOWER':votepower,
        'FULLVALUE':voteval,
        "SLIDERFORM":votewt,
        "ACTIVITY_TYPE":activity_type,
        "SORT_TYPE":sort,
        }
    return HttpResponse(template.render(context,request))

def browse(request, steem_id):
    s = Steem()
    template = loader.get_template('activities/left-sidebar.html', using='backend')
    user = steem_id
    context = {}
    account = s.get_account(user)
    profile_image = "TEMPLATE IMAGE"
    profile_about = ""
    profile_location = ""
    profile_banner = ""
    recentActivities = []
    recentPosts = []
    postList = s.get_blog(str(user),-1,30)
    postList = list(reversed(postList))
    postList = postList[:int(len(postList)/2)]
    postList = list(reversed(postList))
    newList = []
    for post in postList:
        json_data = json.loads(post['comment']['json_metadata'])
        if 'exhaust' in json_data['app'] or post['comment']['author']!=str(user):
             pass
#            print("Not adding post: " + str(post['comment']['title']))
        else:
#            print("Adding post: " + str(post['comment']['title']))
            newList.append([post,humanize.naturaldelta(datetime.strptime(str(post['comment']['created']),'%Y-%m-%dT%H:%M:%S'))])
    newList = newList[:5]
    runList = {}
    context = {
        'profile_image': "https://openclipart.org/download/236194/1452091957.svg",
        'about': "My profile ain't none of yo business",
        'location': "UNDISCLOSED",
        'cover_image': "null",
    }
    users = User.objects.all()
    userList = []
    for name in users:
        userList.append(name.username)
    if user in userList:
        username = User.objects.get(username=user)
        runList = Activity.objects.filter(user=username)
        runList = list(reversed(runList))[:20]
    else:
        runList="No Activities Yet!"
    if 'profile' in account['json_metadata']:
        account_info = json.loads(account['json_metadata'])
        for field in context:
            if field in account_info['profile']:
                context[field] = account_info['profile'][field]
    context['STEEM_ID'] = user
    context['RUNLIST']=runList
    context['POSTLIST']=newList
    context['NUMPOSTS']=len(newList)
    return HttpResponse(template.render(context,request))

@login_required
def test_new_upload(request, activity_type):
    user = User.objects.get(username=request.user)
    steem_id = user.username
    account_id = User.objects.get(username = steem_id)
    splits = []
    context = {}
    activityList = {
        'run':{'NICK':'Running','PATH':"documents/runs"},
        'cycle':{'NICK':'Cycling','PATH':"documents/cycles/"},
        'hike':{'NICK':'Hiking', 'PATH':"documents/hikes/"},
        'climb':{'NICK':'Climbing', 'PATH':"documents/climbs/"},
        'yoga':{'NICK':'Yoga', 'PATH':"documents/yoga/"},
        'strength':{'NICK':'Strength Training', 'PATH':"documents/strength/"},
        'surf':'Session',
        }
    ########################################################
    #### Check if it's a Distance Sport, or a "Workout" ####
    #### Set Appropriated Template                      ####
    ########################################################

    ########################
    #### Data Submitted ####
    ########################
    if request.method == 'POST':
        ##################################
        #### If GPS File was Included ####
        ##################################
        if 'gpsfile' in request.FILES:
            if activity_type == 'run':
                type = activityType.objects.get(type='running')
            if activity_type == 'cycle':
                type = activityType.objects.get(type='cycling')
            if activity_type == 'hike':
                type = activityType.objects.get(type='hiking')
            if activity_type == 'climb':
                type = activityType.objects.get(type='climbing')
            activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'], gpsfile=request.FILES['gpsfile'],uploaded_at=datetime.now(), tags = request.POST['tags'])
            activity.save()
            ################################
            #### Handle Photos 1 thru 3 ####
            ################################
####### PHOTO HANDLER SHOULD GET MOVED TO END OF UPLOAD METHOD. UNIVERSAL TO ALL ACTIVITIES.
            if 'photo1' in request.FILES:
                activity.photo = request.FILES['photo1']
                activity.save()
                downscale(
                    str(activity.photo.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-"+str(activity.pk)
                )
                if '.gif' in activity.photo.name:
                    pass
                else:
                    activity.photo = activityList[activity_type]['PATH']+str(account_id)+"-"+str(activity.pk)+".jpg"
                    activity.save()
            else:
                activity.photo = activity_type + '-default.jpg'
                activity.save()
            if 'photo2' in request.FILES:
                activity.photo2 = request.FILES['photo2']
                activity.save()
                downscale(
                    str(activity.photo2.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-2-"+str(activity.pk)
                )
                if '.gif' in activity.photo.name:
                    pass
                else:
                    activity.photo2 = activityList[activity_type]['PATH']+str(account_id)+"-2-"+str(activity.pk)+".jpg"
                    activity.save()
            if 'photo3' in request.FILES:
                activity.photo3 = request.FILES['photo3']
                activity.save()
                downscale(
                    str(activity.photo3.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-3-"+str(activity.pk)
                )
                if '.gif' in activity.photo3.name:
                    pass
                else:
                    activity.photo3 = activityList[activity_type]['PATH']+str(account_id)+"-3-"+str(activity.pk)+".jpg"
                    activity.save()
            pointList = []
            ################################
            #### Handle GPX Track Files ####
            ################################
            if ".gpx" in str(activity.gpsfile.name):
                distance, duration, pace, points, times, splits = importGPX('media/'+str(activity.gpsfile.name))
                activity.distance = distance
                activity.duration = duration
                activity.pace = pace
                activity.save()
                pointList.clear()
            ################################
            #### Handle TCX Track Files ####
            ################################
            else:
                avgHR = 0
                maxHR = 0
                points, avgpace, avgHR, maxHR, ascent, completed, distance, duration, splits = importTCX('media/'+str(activity.gpsfile.name))
                activity.distance = distance
                activity.duration = duration
                activity.save()
        ##############################
        #### No GPS File Included ####
        ##############################
###### THIS ENTIRE SECTION CAN GET TRIMMED DOWN SIGNIFICANTLY. REMOVE PHOTO HANDLERS. UNIVERSAL TO ALL.
        else:
            timer = re.findall('\d{1,2}',request.POST['duration'])
            duration = 0
            mult = 2
            for time in timer:
                duration += int(time) * int(math.pow(60,int(mult)))
                mult -= 1
            if activity_type == 'run':
                type=activityType.objects.get(type='running')
                if request.FILES:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration, tags=request.POST['tags'])
                    activity.pace = str((activity.duration/60)/(activity.distance/1000))
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    else:
                        activity.photo = activity_type + '-default.jpg'
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration, tags=request.POST['tags'])

            if activity_type == 'cycle':
                type=activityType.objects.get(type='cycling')
                if request.FILES:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration, tags=request.POST['tags'])
                    activity.pace = str((activity.duration/60)/(activity.distance/1000))
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    else:
                        activity.photo = activity_type + '-default.jpg'
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration, tags=request.POST['tags'])

            if activity_type == 'hike':
                type=activityType.objects.get(type='hiking')
                if request.FILES:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration, tags=request.POST['tags'])
                    activity.pace = str((activity.duration/60)/(activity.distance/1000))
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    else:
                        activity.photo = activity_type + '-default.jpg'
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Activity(user=account_id,type=type, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration, tags=request.POST['tags'])

            if activity_type == 'yoga':
                type=activityType.objects.get(type='yoga')
                if request.FILES:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), duration=duration, tags=request.POST['tags'], video=request.POST['video_URL'], coach=request.POST['instructor'])
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    else:
                        activity.photo = activity_type + '-default.jpg'
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Activity(user=account_id,type=type, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), duration=duration, tags=request.POST['tags'], style=request.POST['style'], video=request.POST['video_URL'],coach=request.POST['instructor'])

            if activity_type == "strength":
                type = activityType.objects.get(type="strength")
                if request.FILES:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), duration=duration, tags=request.POST['tags'], muscles=request.POST['muscle_group'], equipment=request.POST['equipment_used'])
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    else:
                        activity.photo = activity_type + '-default.jpg'
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Activity(user=account_id, type=type, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), duration=duration, tags=request.POST['tags'], muscles=request.POST['muscle_group'], equipment=request.POST['equipment_used'])

            activity.save()
    ##################################
    #### Down-Res Uploaded Images ####
    ##################################
            if activity.photo:
                downscale(
                    str(activity.photo.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-"+str(activity.pk)
                )
                if '.gif' in activity.photo.name:
                    pass
                else:
                    activity.photo = activityList[activity_type]['PATH']+str(account_id)+"-"+str(activity.pk)+".jpg"
                    activity.save()
            else:
                activity.photo = activity_type + '-default.jpg'
                activity.save()
            if activity.photo2:
                downscale(
                    str(activity.photo2.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-2-"+str(activity.pk)
                )
                if '.gif' in activity.photo2.name:
                    pass
                else:
                    activity.photo2 = activityList[activity_type]['PATH']+str(account_id)+"-2-"+str(activity.pk)+".jpg"
                    activity.save()
            if activity.photo3:
                downscale(
                    str(activity.photo3.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-3-"+str(activity.pk)
                )
                if '.gif' in activity.photo3.name:
                    pass
                else:
                    activity.photo3 = activityList[activity_type]['PATH']+str(account_id)+"-3-"+str(activity.pk)+".jpg"
                    activity.save()
    ##################################
    #### REDIRECT TO NEW ACTIVITY ####
    ##################################
        return redirect('/dash/'+activity_type+'/'+str(activity.pk)+'/')
    ##########################
    #### Initialize Forms ####
    ##########################
    else:
        dataform = DataForm()
        photoform = PhotoForm()
        commentform = CommentForm()
        datepicker = DatePicker()
        gpsform = GPSForm()
        tagform = TagForm()
        yogaform = YogaForm()
        strengthform = StrengthForm()
    ############################
    #### Initialize Context ####
    ############################
    if activity_type == "yoga" or activity_type == "strength":
        template=loader.get_template('activities/WorkoutUploader.html',using='backend')
        context = {
            'photoform':photoform,
            'yogaform': yogaform,
            'strengthform': strengthform,
            'commentform':commentform,
            'tagform': tagform,
            'ACTIVITY': activityList[activity_type]['NICK'],
        }
    else:
        template=loader.get_template('activities/GPSuploader.html',using='backend')
        context = {
            'dataform':dataform,
            'photoform':photoform,
            'commentform':commentform,
            'datepicker':datepicker,
            'gpsform': gpsform,
            'tagform': tagform,
            'ACTIVITY': activityList[activity_type]['NICK'],
        }

    return HttpResponse(template.render(context,request))

def getReplies(replies, user):
    comments = {}
    voterList = []
    markdowner = Markdown()
    counter = 0
    for comment in replies:
        subcomments = {}
        voted = False
        votes = 0
        commentpost = Post('@'+comment.author+'/'+comment.permlink)
        for voter in commentpost['active_votes']:
            voterList.append(voter['voter'])
        if str(user) in voterList:
            voted = True
        votes = len(voterList)
        subs = list(Post.get_replies(comment))
        for sub in subs:
            subvoterList = []
            subvoted = False
            for voter in sub['active_votes']:
                subvoterList.append(voter['voter'])
            if str(user) in subvoterList:
                subvoted = True
            subvotes = len(subvoterList)
            subcomments[counter] = {
                'AUTHOR': sub.author,
                'BODY': markdowner.convert(sub.body),
                'PERMLINK': sub.permlink,
                'REWARD': sub.reward,
                'VOTES': subvotes,
                'VOTED': subvoted,
            }
            counter+=1
        comments[counter] = {
            'AUTHOR': comment.author,
            'BODY': markdowner.convert(comment.body),
            'PERMLINK': comment.permlink,
            'REWARD': comment.reward,
            'VOTES': votes,
            'VOTED': voted,
            'REPLIES': subcomments,
            'NUMREPLIES': len(subcomments),
        }

        voterList.clear()
        counter += 1
        ### NEED TO ADD SUPPORT FOR SUB-COMMENTS ###
    return(comments)

def event_detail(request, event_id):
    template = loader.get_template('activities/event_detail.html', using='backend')
    try:
        user = User.objects.get(username=request.user)
    except:
        user = 'null'
    event = Event.objects.get(pk=event_id)
    markdowner = Markdown()
    eventBody = markdowner.convert(event.post_body)
    soup = BeautifulSoup(eventBody, 'html.parser')
    results = Result.objects.filter(event=event)
    imglinks = []
    if event.photo:
        imglinks.append([0,event.photo.url])
    if event.photo2:
        imglinks.append([1,event.photo2.url])
    if event.photo3:
        imglinks.append([2,event.photo3.url])

    context = {
        'USER':user,
        'RESULTS':results,
        'EVENT':event,
        'BODY':soup,
        'IMG_LINKS':imglinks
    }
    return HttpResponse(template.render(context,request))

def goldencheetah(gpsfile):
    pass

def gps_detail(request, activity_type, activity_id):
    template = loader.get_template('activities/run_detail.html', using='backend')
    try:
        user = User.objects.get(username=request.user)
    except:
        user = 'null'
    ##############################
    #### Initialize Variables ####
    ##############################
    filename =  'null'
    param = 0
    splitDist = 1000
    smoothDist = 50
    distTotal = 0.0
    sample=5
    splitCurr = 0.0
    elev = []
    pace = []
    heart = []
    cadence = []
    distance = 0.0
    heartchart = []
    maxHR = 0
    avgHR = 0
    avgpace = ''
    ascent = 0
    completed=''
    splits = []
    cadenceHTML = ''
    heartHTML = ''
    paceHTML=''
    elevHTML=''
    splitHTML=''
    wattHTML=''
    speedHTML=''
    ytHash = '' #hash code for youtube video embed
    center = []
    linestring = []
    activity = Activity.objects.get(pk=activity_id)
    type = activity.type
    showmap = activity.showmap
    markdowner = Markdown()
    reward = 0.0
    voterList = []
    viewer = request.user
    ###########################
    #### Set Activity Type ####
    ###########################
#    if activity_type == 'cycle' or activity_type == 'cycling':
#        type = activityType.objects.get(type="cycling")
#    if activity_type == 'running' or activity_type == 'run':
#        type = activityType.objects.get(type="running")
#    if activity_type == 'hiking' or activity_type == 'hike':
#        type = activityType.objects.get(type="hiking")
#    if activity_type == 'yoga':
#        type = activityType.objects.get(type="yoga")
    ##################################################
    #### Check to See if Activity Posted to Steem ####
    ##################################################
    if activity.posted == True:
        post = Post(activity.permlink)
        replies = list(Post.get_replies(post))
        if len(replies)>=1:
            comments = getReplies(replies, user)
        else:
            comments = []
        reward = post.reward
        activity.rewards = float(re.sub(' SBD','',str(reward)))
        activity.save()
        for voter in post['active_votes']:
            voterList.append(voter['voter'])
        activity.votes = len(voterList)
        activity.voters = voterList
        try:
            if str(user.username) in voterList:
                activity.voted = True
        except:
            pass
    else:
        comments = {}
    ########################################
    #### If Activity DOES Have GPS File ####
    ########################################
    if activity.gpsfile: 
        filename = 'media/'+str(activity.gpsfile.name)
        if 'gpx' in filename:
            print("GPX File Format Detected")
            gpx_file = open(filename,'r')
            gpx = gpxpy.parse(gpx_file)
            counter = 0
            bounds = gpx.get_bounds()
            lat_cen = (bounds.min_latitude+bounds.max_latitude)/2
            lon_cen = (bounds.min_longitude+bounds.max_longitude)/2
            center = [lon_cen,lat_cen]
            for point in gpx.get_points_data():
#                if counter%5==0:
                linestring.append([point[0].longitude,point[0].latitude])
                counter += 1
            distance, duration, pacecalc, points, timestart, splits = importGPX(filename)
            initTime = timestart.timestamp()
            minutes = str(int(pacecalc)) 
            seconds = (int((pacecalc%1)*60))
            seconds = format(seconds, '02d')
            avgpace = minutes+':'+seconds
            activity.pace = avgpace
            activity.save()
            avgHR = 'NO HR DATA'
            maxHR = 'NO HR DATA'
            for i in range(1, len(points)):
                splitCurr += (points[i]).distance_from_start-points[i-1].distance_from_start
                if splitCurr >= smoothDist:
                    distance = round((points[i].distance_from_start)/1000,2)
                    pace.append([distance, ((points[i].point.time.timestamp()-initTime)/60)/distance])
                    elev.append([distance, (points[i].point.elevation)])
                    splitCurr = 0.0
            paceHTML = paceChart2(points)
            elevHTML = elevChart(points)
            splitHTML = splitChart(splits)
            points.clear()
        else:
            tcx = tcxparser.TCXParser(filename)
            counter = 0
            for lap in tcx.activity.Lap:
                for point in lap.Track.Trackpoint:
                    if counter%10==0:
                        try:
                            linestring.append([point.Position.LongitudeDegrees,point.Position.LatitudeDegrees])
                        except:
                            linestring.append([(counter/180),(counter/180)])
                    counter += 1
            lats = []
            lons = []
            for point in linestring:
                lons.append(point[0])
                lats.append(point[1])
            cen_lon = (max(lons)+min(lons))/2
            cen_lat = (max(lats)+min(lats))/2
            center = [cen_lon,cen_lat]
            print("TCX File Format Detected")
            points, avgpace, avgHR, maxHR, ascent, completed, length, duration, splits = importTCX(filename)
            activity.pace = avgpace
            activity.ascent = ascent
            try:
                if str(tcx.activity.Notes) != "":
                    activity.comments = str(tcx.activity.Notes)
            except:
                pass
            activity.save()
            for i in range(1,len(points)):
                splitCurr += (points[i][0]-points[i-1][0])
                if splitCurr >= smoothDist:
   #                 distance = round((points[i][0])/1000,2)
   #                 pace.append([distance, ((points[i][1]-points[i-smoothDist][1])/60)/((points[i][0]-points[i-smoothDist][0])/1000)])
   #                 elev.append([distance, points[i][2]])
                    splitCurr = 0.0
            paceHTML = paceChart2(points)
            cadenceHTML = cadenceChart(points)
            elevHTML = elevChart(points)
            heartHTML = heartChart(points)
            splitHTML = splitChart(splits)
            wattHTML = wattageChart(points)
            speedHTML = speedChart(points)
            points.clear()
    ####################################
    #### If NO GPS File w/ Activity ####
    ####################################
    else:
        if (type.type == "running") or (type.type == "hiking") or (type.type == "cycling"):
            activity.pace = round((activity.duration/60)/(activity.distance/1000),2)
            minutes = str(int(activity.pace))
            seconds = int((activity.pace%1)*60)
            seconds = format(seconds, '02d')
            activity.pace = minutes+':'+seconds
            avgpace = activity.pace
            activity.save()
            showmap = 0
        if type.type == "yoga" or type.type == "strength":
            ### Check for video host
            if activity.video:
                if "youtube" in activity.video:
                    ytHash = re.sub('h.+v=','',activity.video)
#                    ytHash = "FUCK"
            template = loader.get_template('activities/workout_detail.html', using='backend')
    ################################
    #### Add Photos to Carousel ####
    ################################
    imglinks = []
    if activity.photo:
        imglinks.append([0,activity.photo.url])
    else:
        activity.photo = activity_type+"-default.jpg"
        activity.save()
        imglinks = [0,activity.photo.url]
    if activity.photo2:
        imglinks.append([1,activity.photo2.url])
    if activity.photo3:
        imglinks.append([2,activity.photo3.url])
    #############################
    #### Handle New Comments ####
    #############################
    if request.method == 'POST':
        commenter = CommentForm(request.POST)
        if commenter.is_valid():
            body = commenter.cleaned_data['description']
            createComment(request, str(activity.user),str(activity.permlink), str(body))
    else:
        commenter = CommentForm()
    ################################
    #### Manages Activity Notes ####
    ################################
    markdowner = Markdown()
    activityBody = markdowner.convert(activity.comments)
    soup = BeautifulSoup(activityBody, 'html.parser')
    ###############################################
    #### Find Users Beneficiary Rate for Sport ####
    ###############################################
    try:
        taxes, events = check_tax(user,type)
    except:
        taxes = 0.20
    ###################################
    #### Get Steem Account Details ####
    ###################################
    beem = Beem()
    if str(request.user) != "AnonymousUser":
        account = Account(str(request.user))
        vests = round(beem.vests_to_sp(account.get_vests()),3)
        votepower = str(round(account.get_voting_power(),2))+"%"
        voteval = str(round(account.get_voting_value_SBD(),3))
#         vests = "NULL"
#         votepower = "NULL"
#         voteval = "NULL"
    else:
        account = 'null'
        vests= 100
        votepower = 100
        voteval = 0
    ####################################
    #### Add Slider for Vote Weight ####
    ####################################
    if request.method == 'POST':
        votewt = SlideForm(request.POST)
    else:
        votewt = SlideForm()
    #######################################
    #### Populate Context for Template ####
    #######################################
    ######################################################################
    #### This should be updated to only send context needed for sport ####
    ######################################################################
    context={
        "RUNNER": str(activity.user),
        "TAXES": taxes,
        "VIEWER": str(viewer),
        "RUN_DATE": humanize.naturaldate(activity.start),
        "RUN_NOTES": soup,
        "DISTANCE": str(activity.distance/1000),
        "HOURS": int(activity.duration/60/60),
        "MINUTES": int(activity.duration%3600/60),
        "SECONDS": activity.duration%60,
        "IMG_LINKS": imglinks,
        "RUN":activity,
        "SUBMITTED":activity.posted,
        "COMMENTS":comments,
        "COMCOUNT":len(comments),
        "COMMENTER":commenter,
        "RESPONDER":ReplyForm(),
        "REWARD":reward,
#        "GEODATA":geodata,
        "AVGPACE": avgpace,
        "HRMAX": maxHR,
        "HRAVG": avgHR,
        "ASCENT": ascent,
        "PACECHART":paceHTML,
        "ELEVCHART":elevHTML,
        "HEARTCHART": heartHTML,
        "CADENCECHART": cadenceHTML,
        "SPLITCHART":splitHTML,
        "WATTCHART":wattHTML,
        "SPEEDCHART":speedHTML,
        'SPLIT_TIME': param, ## split time (y-axis, bar chart, normalized to min/km "6:01")
        'SPLITS': splits, ## split No. (x-axis, bar chart, "1,2,3,4...")
        'ELEV': elev, ## elevation data (y-axis, line chart, "25m")
        'PACE': pace, ## pace data (y-axis, line chart, min/km "5:50")
        'DIST': distance, ## distance list (x-axis, line chart, cumulative distance)
        'HEART': heart,
        'STEEM_ID': str(user),
        'SMOOTH': smoothDist,
        'ACTIVITY_TYPE': activity_type,
        'VESTS':vests,
        'VOTINGPOWER':votepower,
        'FULLVALUE':voteval,
        "SLIDERFORM":votewt,
        "CENTER":center,
        "LINESTRING": linestring,
        "SHOWMAP": showmap,
        "YOUTUBE": ytHash,
        }
    return HttpResponse(template.render(context,request))

def check_token_expiry(scUser):
    now = round(datetime.now().timestamp())
    print("Checking token")
    print(now, scUser.expiry)
    if now >= scUser.expiry:
        print("Token expired! Updating token!")
        try:
            response = scUser.update_access_token(settings.APP_SECRET)
            print(response)
            if response.status_code == 200:
                scUser.save()
        except:
            print('Invalid access token')
            return redirect('/accounts/login/')
    else:
        print("Token still valid! Worry about it later!")

@login_required
def event_post(request, event_id):
    user=User.objects.get(username=request.user)
    steemUser = SteemConnectUser.objects.get(user=user)
    event = Event.objects.get(pk=event_id)
    check_token_expiry(steemUser)
    tags = ['exhaust','running','race','equinox2019','fivekm']
    permalink = 'exhaust-event-' + str(event_id)
    comment = Comment(author=str(user.username),
                  parent_permlink=tags[0],
                  parent_author="",
                  permlink=str(permalink),
                  body=str(event.post_body),
                  title = str(event.name),
                  json_metadata={
                      "app":"exhaust/0.2a",
                      "tags":[
                          tags[1],
                          tags[2],
                          tags[3],
                          tags[4]
                      ]
                  })
    if user.username != 'exhaust':
        benef = [
            {'account': 'exhaust', 'weight': 2000},
        ]
        comment_options = CommentOptions(parent_comment=comment, beneficiaries=benef)
        sc2 = SteemConnect(steemUser.access_token, comment_options.operation)
    else:
        sc2 = SteemConnect(steemUser.access_token, comment.operation)
    response = sc2.run
    if response.status_code == 200:
        event.posted = True
        event.permlink = '@' + str(request.user) + '/' + str(permalink)
        event.save()
        return redirect('/')
    else:
        return HttpResponse(response)
        return redirect('/accounts/login/')


def check_tax(user, type):
    events = Event.objects.filter(participants__username=user.username, closed=False)
    events = events.filter(event_type=type)
    tax = 0.00
    for event in events:
        tax += event.event_tax
#        print(event.name + " -- fee : " + str(event.event_tax))
#    print("Tax rate from events = " + str(tax))
    if tax > 0.05:
        tax = tax + 0.15
    else:
        tax = 0.20
#    print("Nominal tax rate = " + str(tax))
    return tax, events

@login_required
def create_post(request, activity_type, activity_id):
    user = User.objects.get(username=request.user)
    steemUser = SteemConnectUser.objects.get(user=user)
    check_token_expiry(steemUser)
    activity = Activity.objects.get(pk=activity_id)
    type = ''
    body_text = ''
    if activity_type == 'run':
        type = activityType.objects.get(type='running')
    if activity_type == 'hike':
        type = activityType.objects.get(type='hiking')
    if activity_type == 'cycle':
        type = activityType.objects.get(type='cycling')
    if activity_type == 'yoga':
        type = activityType.objects.get(type='yoga')
    if activity_type == 'strength':
        type = activityType.objects.get(type='strength')
    linktime = ('exhaust-'+str(activity_type)+'-'+str(int(activity.uploaded_at.timestamp())))
    tagstring = activity.tags
    tags = re.findall('[0-9a-zA-Z-]+',tagstring)
    if len(tags)<1:
        tags[0]='exhaust'
        tags[1]='fitnation'
        tags[2]=str(activity.type)
        tags[3]='training'
        tags[4]='XHST'
    if len(tags)<5:
        for i in range(0,4):
            tags.append('')
    if activity_type == 'yoga':
        body_text = ("I just finished a " +
                    activity_type +
                    " activity that lasted about " +
                    str(int(activity.duration/60/60)) +
                    "hh:" + str(int(activity.duration%3600/60)) +
                    "mm:" + str(activity.duration%60) +
                    "ss !<br><center>![image](https://xhaust.me" + activity.photo.url + ")</center><br>" + activity.comments)
    elif activity_type == 'strength':
        body_text = ("I just finished a " +
                    activity_type +
                    " training activity that lasted about " +
                    str(int(activity.duration/60/60)) +
                    "hh:" + str(int(activity.duration%3600/60)) +
                    "mm:" + str(activity.duration%60) +
                    "ss !<br><center>![image](https://xhaust.me" + activity.photo.url + ")</center><br>" + activity.comments)
    else:
        body_text = ("I just finished a " +
                    str(activity.distance/1000) +
                    "km " +
                    activity_type +
                    " that lasted about " +
                    str(int(activity.duration/60/60)) +
                    "hh:" + str(int(activity.duration%3600/60)) +
                    "mm:" + str(activity.duration%60) +
                    "ss !<br><center>![image](https://xhaust.me" + activity.photo.url + ")</center><br>" + activity.comments)
    if activity.photo2:
        body_text += "<br><center>![image](https://xhaust.me" + activity.photo2.url + ")</center><br>"
    if activity.photo3:
        body_text += "<br><center>![image](https://xhaust.me" + activity.photo3.url + ")</center><br>"
    body_text += "<br>Check out some detailed info at [my EXHAUST page](https://xhaust.me/feed/"+str(activity_type)+"/"+str(activity_id)+"/)"+"<br>Join me in testing out [EXHAUST](https://xhaust.me)!"
    comment = Comment(author=str(activity.user),
                      parent_permlink=tags[0],
                      parent_author="",
                      permlink=str(linktime),
                      body=str(body_text),
                      title = str(activity.title),
                      json_metadata={
                          "app":"exhaust/0.2a",
                          "tags":[
                              tags[1],
                              tags[2],
                              tags[3],
                              tags[4]
                          ]
                      })
    taxes, events = check_tax(user,type)
    taxes = taxes * 10000
    benef = [
        {'account': 'exhaust', 'weight': taxes},
    ]
    comment_options = CommentOptions(parent_comment=comment, beneficiaries=benef)
    sc2 = SteemConnect(steemUser.access_token, comment_options.operation)
    response = sc2.run
    if response.status_code == 200:
        activity.posted = True
        activity.permlink = '@' + str(activity.user) + '/' + str(linktime)
        activity.save()
        queue = VoteQueue(author=str(activity.user),permlink=str(linktime),time_added=round(datetime.now().timestamp()), delay=360)
        queue.save()
        commenter = Commenter(activity=activity)
        commenter.save()
        return redirect('/dash/' + activity_type + '/' +str(activity.pk) + '/')
    else:
        return HttpResponse(response)
        return redirect('/accounts/login/')

def vote_activity(request, activity_id, weight):
    steem_id = User.objects.get(username=request.user)
    steemUser = SteemConnectUser.objects.get(user=steem_id)
    check_token_expiry(steemUser)
    activity = Activity.objects.get(pk=activity_id)
    activity_type = activity.type.type
    link = activity.permlink
    permlink = re.sub('@'+str(activity.user)+'/','',link)
    vote = Vote(voter=steem_id.username,author=activity.user.username,permlink=permlink,weight=weight)
    op = vote.operation
    sc2 = SteemConnect(steemUser.access_token, op)
    sc2.run
    return redirect('/feed/' + str(activity_type) + '/' + str(activity_id) +'/')

def vote_comment(request, author, comment, weight):
    steem_id = User.objects.get(username=request.user)
    steemUser = SteemConnectUser.objects.get(user=steem_id)
    check_token_expiry(steemUser)
    vote = Vote(voter = str(steem_id.username),author = str(author),permlink = comment,weight =weight)
    op = vote.operation
    sc2 = SteemConnect(steemUser.access_token, op)
    response = sc2.run
    return redirect('/article/@'+str(author)+'/'+str(comment))

def createComment(request, pauthor, plink, body):
    user = User.objects.get(username=request.user)
    steemUser = SteemConnectUser.objects.get(user=user)
    check_token_expiry(steemUser)
    json_data = {}
    json_data["app"]="exhaust/0.1a"
    body_text = body
    parent_permlink = re.sub('@.{1,16}/','',plink)
    parent_author = pauthor
    plink_auth = re.sub("\.","",str(user.username))
    permlink = (str(plink_auth)+'-comment-'+str(int(datetime.now().timestamp())))
    comment = Comment(author=str(request.user),
                      parent_permlink = str(parent_permlink),
                      parent_author = str(parent_author),
                      title='exhaust-comment',
                      permlink=str(permlink),
                      body=str(body_text),
                      json_metadata=json_data)
    benef = {}
    benef['account']='exhaust'
    benef['weight']=500
    benef=[benef]
    options = CommentOptions(parent_comment=comment,beneficiaries= benef)
    sc2 = SteemConnect(steemUser.access_token,options.operation)
    sc2.run
    queue = VoteQueue(author=user.username,permlink=permlink,time_added=round(datetime.now().timestamp()), delay=360)
    queue.save()
    return redirect('/feed/')

def share_post(request, run_id):
    return HttpResponse("Hello -- You should be able to resteem posts with this button soon.")
