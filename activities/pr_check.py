from datetime import datetime
from django.contrib.auth.models import User


from activities.models import Action, activityType

users = User.objects.all()

def running_records(user):
    actType = activityType.objects.get(type='running')
    longest_run = ''
    onek_run_pr = ''
    fivek_run_pr = ''
    tenk_run_pr = ''
    half_run_pr = ''
    full_run_pr = ''
    longest_cycle = ''
    fastest_run = ''
    fastest_cycle = ''
    seven_day_dist = 0.0
    thirty_day_dist = 0.0
    ninety_day_dist = 0.0
    runs = Action.objects.filter(user=user, type=actType)
    if len(runs)==0:
        return("No runs for user " + str(user.username))
    longest = runs.order_by('distance').reverse()
    longest_run = longest[0]
    fastest = runs.order_by('pace')
    fastest_run = fastest[0]
    fivers = runs.filter(distance__lte=5100,distance__gte=4900).order_by('pace')
    if len(fivers) == 0:
        pass
    else:
        fivek_run_pr = fivers[0]
    teners = runs.filter(distance__lte=10500,distance__gte=9500).order_by('pace')
    if len(teners) == 0:
        pass
    else:
        tenk_run_pr = teners[0]
    halfs = runs.filter(distance__lte=22000,distance__gte=19900).order_by('duration')
    if len(halfs) == 0:
        pass
    else:
        half_run_pr = halfs[0]
    fulls = runs.filter(distance__lte=42000,distance__gte=43000).order_by('duration')
    if len(fulls) == 0:
        pass
    else:
        full_run_pr = fulls[0]
    print(str(user.username)+":")
    print("Distance PR: " + str(longest_run.title) + " - " + str(longest_run.distance))
    print("Pace PR: " + str(fastest_run.title) + " - " + str(fastest_run.pace))
    if fivek_run_pr != "":
        print("5k PR: " + str(fivek_run_pr.title) + " - " + str(fivek_run_pr.duration))
    if tenk_run_pr != "":
        print("10k PR: " + str(tenk_run_pr.title) + " - " + str(tenk_run_pr.duration))
    if half_run_pr != "":
        print("Half Marathon PR: " + str(half_run_pr.title) + " - " + str(half_run_pr.duration))
    if full_run_pr != "":
        print("Marathon PR: " + str(full_run_pr.title) + " - " + str(full_run_pr.duration))

    last_seven = runs.filter(start__gte=datetime.fromtimestamp(datetime.now().timestamp()-7*24*3600))
    last_thirty = runs.filter(start__gte=datetime.fromtimestamp(datetime.now().timestamp()-30*24*3600))
    last_ninety = runs.filter(start__gte=datetime.utcfromtimestamp(datetime.utcnow().timestamp()-90*24*3600))
    for run in last_seven:
        seven_day_dist+=run.distance/1000
    for run in last_thirty:
        thirty_day_dist+=run.distance/1000
    for run in last_ninety:
        ninety_day_dist+=run.distance/1000
    print("Weekly: " + str(seven_day_dist))
    print("Monthly: " + str(thirty_day_dist))
    print("3 Month: " + str(ninety_day_dist))
