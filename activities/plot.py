import matplotlib.pyplot as plt, mpld3
import numpy as np

from activities.gpxParse import importTCX
from activities.models import Action, activityType

from datetime import datetime
from django.contrib.auth.models import User
from mpld3 import plugins

def fromTCX():
    pointList, pace, avgHR, maxHR, ascent, runDate, distance, dration, splits = importTCX('runner95.tcx')
    return(pointList)

plt.rcParams['toolbar'] = 'None'

def splitChart(splits):
    fig, ax = plt.subplots(figsize=(10,2))
    splitID = np.empty(shape=(0,0))
    splitTime = np.empty(shape=(0,0))
    for i in range(0,len(splits)):
        splitID = np.append(splitID,splits[i][0])
        splitTime = np.append(splitTime,splits[i][1])
    plt.bar(splitID,splitTime)
    plt.xlabel('Km')
    plt.ylabel('Time (min)')
    plt.title('1000m Splits')
    splitHTML=mpld3.fig_to_html(fig)
    return(splitHTML)

def paceChart2(points):
    prevdist = 0
    prevtime = 0
    prevpace = 0
    num = len(points)
    smooth = 30
    x1 = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Pace array y1-axis
    try:
        starttime = int(points[0][1]) #Timestamp at STart
    except:
        starttime = int(points[0].point.time.timestamp())
    for i in range(0,num-1):
        lookback = 1
        if i >= smooth:
            lookback = smooth
        try:
            distance = (points[i][0])/1000 #Cumulative Distance
            time = (points[i][1]-starttime)/60 #Cumulative time
            if i == 0:
                prevdist = 0
                prevtime = 0
                pace = 0
                continue
            else:
                prevdist = (points[i-lookback][0])/1000 #Previous Distance
                prevtime = (points[i-lookback][1]-starttime)/60 #Previous Time
        except TypeError:
            distance = round((points[i].distance_from_start)/1000,2)
            time = (points[i].point.time.timestamp() - starttime)/60
            if i == 0:
                prevdist = 0
                prevtime = 0
                pace = 0
                continue
            else:
                prevdist = round(points[i-lookback].distance_from_start/1000,2) #Previous Distance
                prevtime = (points[i-lookback].point.time.timestamp() - starttime)/60
        deltadist = distance - prevdist #Distance travelled since last point
        deltatime = time - prevtime #Time passed since last point
        try:
            pace = ((deltatime / deltadist)+prevpace)/2 # Pace over last interval
            prevpace = pace
        except ZeroDivisionError:
            pass
        prevdist = distance
        prevtime = time
        x1 = np.append(x1,distance)
        y1 = np.append(y1,pace)
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title("Pace")
    plt.ylabel('Pace (min/km)')
    plt.xlabel('Distance (km)')
    ax.plot(x1,y1,'b')
    ax.set_ylim(1.5,y1.mean()*1.5)
    paceHTML = mpld3.fig_to_html(fig)
    return(paceHTML)



def paceChart(points):
    prevdist = 0 # Initialize
    prevtime = 0 # Initialize
    prevpace = 0 # Initialize
    num = len(points) # Number of points  to process
#    smooth = 5
    smooth = int(num/1000) # Ratio to trim graph to n=1000
    if smooth == 0:
        smooth = 1
    x = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Elevation array y1-axis
    y2 = np.empty(shape=(0,0)) # Pace array y2-axis
    for i in range(0,num-1):
        if i % int(smooth) == 0:
            try:
                starttime = int(points[0][1]) # Timestamp at Start
                distance = (points[i][0])/1000 #Cumulative Distance
                time = (points[i][1]-starttime)/60 #Cumulative time
            except TypeError:
                starttime = int(points[0].point.time.timestamp())
                distance = round((points[i].distance_from_start)/1000,2)
                time = (points[i].point.time.timestamp() - starttime)/60
            deltadist = distance - prevdist #Distance travelled since last point
            deltatime = time - prevtime #Time passed since last point
            if i == 0:
                pace = 0
            else:
                try:
                    pace = ((deltatime / deltadist)+prevpace)/2 # Pace over last interval
                    prevpace = pace
                except ZeroDivisionError:
                    pass
#                    pace = 10.0 # Pace = 10 if deltadist = 0
            prevdist = distance
            prevtime = time
            x = np.append(x,distance)
#            y1 = np.append(y1,points[i][2])
            y2 = np.append(y2,pace)
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title("Pace")
    plt.ylabel('Pace (min/km)')
    plt.xlabel('Distance (km)')
    ax.plot(x,y2,'b')
    ax.set_ylim(1.5,y2.mean()*1.5)
    paceHTML = mpld3.fig_to_html(fig)
    return(paceHTML)

def elevChart(points):
    num = len(points) # Number of points  to process
    ratio = int(num/500) # Ratio to trim graph to n=1000
    starttime = int(points[0][1]) # Timestamp at Start
    x = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Elevation array y1-axis
    prevelev = points[0][2]
    for i in range(0,num-1):
        if points[i][2]=='NULL':
            elevHTML = "NULL"
            return(elevHTML)
        if i % 5 == 0:
            try:
                distance = round(int(points[i][0])/1000,3) #Cumulative Distance
                currelev = (points[i][2]+prevelev)/2
            except TypeError:
                distance = round((points[i].distance_from_start)/1000,2)
                currelev = points[i].point.elevation
            x = np.append(x,(distance))
            y1 = np.append(y1,currelev)
            prevelev=currelev
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title('Elevation Gain')
    plt.ylabel('Elevation (m)')
    plt.xlabel('Distance (km)')
    ax.plot(x,y1,'g')
    elevHTML = mpld3.fig_to_html(fig)
    return(elevHTML)

def cadenceChart(points):
    num = len(points) # Number of points  to process
    ratio = int(num/500) # Ratio to trim graph to n=5000
    starttime = int(points[0][1]) # Timestamp at Start
    x = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Elevation array y1-axis
    prevcad = points[0][4]
    for i in range(0,num-1):
        if points[i][4]=="NULL":
            cadenceHTML="NULL"
            return(cadenceHTML)
        if i % 5 == 0:
            try:
                distance = round(int(points[i][0])/1000,3) #Current Distance
                currcad = (points[i][4]+prevcad)/2
            except TypeError:
#                distance = round((points[i].distance_from_start)/1000,2) # Something on the trip
                currcad = points[i][4]+prevcad/2
            x = np.append(x,(distance))
            y1 = np.append(y1,currcad)
            prevcad=currcad
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title('Cadence')
    plt.ylabel('Cycling Cadence (rpm)')
    plt.xlabel('Distance (km)')
    ax.plot(x,y1,'p')
    cadenceHTML = mpld3.fig_to_html(fig)
    return(cadenceHTML)

def heartChart(points):
    num = len(points) # Number of points  to process
    ratio = int(num/500) # Ratio to trim graph to n=5000
    starttime = int(points[0][1]) # Timestamp at Start
    x = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Elevation array y1-axis
    prevHR = points[0][3]
    for i in range(0,num-1):
        if points[i][3]=="NULL":
            heartHTML="NULL"
            return(heartHTML)
        if i % 5 == 0:
            try:
                distance = round(int(points[i][0])/1000,3) #Current Distance
                currHR = (points[i][3]+prevHR)/2
            except TypeError:
                distance = round(points[i][0]/1000,2) # Something on the trip
#                currHR = points[i][3]
            x = np.append(x,(distance))
            y1 = np.append(y1,currHR)
            prevHR=currHR
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title('Heart Rate')
    plt.ylabel('Heart Rate (bpm)')
    plt.xlabel('Distance (km)')
    ax.plot(x,y1,'r')
    heartHTML = mpld3.fig_to_html(fig)
    return(heartHTML)

def wattageChart(points):
    num = len(points) # Number of points  to process
    ratio = int(num/500) # Ratio to trim graph to n=5000
    starttime = int(points[0][1]) # Timestamp at Start
    x = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Elevation array y1-axis
    prevWATT = points[0][5]
    for i in range(0,num-1):
        if points[i][5]=="NULL":
            wattHTML="NULL"
            return(wattHTML)
        if i % 5 == 0:
            try:
                distance = round(int(points[i][0])/1000,3) #Current Distance
                currWATT = (points[i][5]+prevWATT)/2
            except TypeError:
#                distance = round((points[i].distance_from_start)/1000,2) # Something on the trip
                currWAT = points[i][5]+prevWATT/2
            x = np.append(x,(distance))
            y1 = np.append(y1,currWATT)
            prevWATT=currWATT
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title('Power Generation')
    plt.ylabel('Workout Power (W)')
    plt.xlabel('Distance (km)')
    ax.plot(x,y1,'y')
    wattHTML = mpld3.fig_to_html(fig)
    return(wattHTML)

def speedChart(points):
    num = len(points) # Number of points  to process
    ratio = int(num/500) # Ratio to trim graph to n=5000
    starttime = int(points[0][1]) # Timestamp at Start
    x = np.empty(shape=(0,0)) # Distance array x-axis
    y1 = np.empty(shape=(0,0)) # Elevation array y1-axis
    try:
        prevSPEED = points[0][6]*3.6
    except:
        speedHTML="NULL"
        return(speedHTML)
    for i in range(0,num-1):
        if points[i][6]=="NULL":
            speedHTML="NULL"
            return(speedHTML)
        if i % 5 == 0:
            try:
                distance = round(int(points[i][0])/1000,3) #Current Distance
#                currSPEED = (points[i][6]+prevSPEED)*3.6/2
                currSPEED = points[i][6]*3.6
            except TypeError:
#                distance = round((points[i].distance_from_start)/1000,2) # Something on the trip
#                currSPEED = (points[i][6]+prevSPEED)*3.6/2
                currSPEED=points[i][6]*3.6
            x = np.append(x,(distance))
            y1 = np.append(y1,currSPEED)
            prevSPEED=currSPEED
    fig, ax = plt.subplots(figsize=(10,2))
    plt.title('Speed')
    plt.ylabel('Speed (km/hr)')
    plt.xlabel('Distance (km)')
    ax.plot(x,y1,'y')
    speedHTML = mpld3.fig_to_html(fig)
    return(speedHTML)

def volumeSeven(userName, acttype):
    user=User.objects.get(username=userName)
    ## Check rolling 7-day cumulative distance over previous 30 days
    now = datetime.now().timestamp()
    chartDistance = []
    x = np.empty(shape=(0,0)) #array of dates
    y1 = np.empty(shape=(0,0)) #array of cumulative distances
    volumeSevenHTML = "null"
    rangeDistance=0.0
    for i in range(-30,1):
        rangeDistance = 0.0
        daysAgo = now + 24*3600*i
        cutoff = daysAgo - 24*3600*7
        actions = Action.objects.filter(user=user, type=acttype, postdate__gte=(datetime.fromtimestamp(cutoff)), postdate__lte=(datetime.fromtimestamp(daysAgo)))
        for action in actions:
            rangeDistance += action.distance/1000
        x = np.append(x,datetime.fromtimestamp(daysAgo))
        y1 = np.append(y1, rangeDistance)
    fig, ax = plt.subplots(figsize=(3,3))
    plt.title("7-Day Cumulative "+str(acttype.type)+" Distance")
    plt.ylabel("Distance (km)")
    plt.xlabel("Date")
    ax.plot(x,y1)
    volumeSevenHTML = mpld3.fig_to_html(fig)
    return(volumeSevenHTML)

def volumeThirty(userName, acttype):
    user=User.objects.get(username=userName)
    ## Check rolling 30-day cumulative distance over previous 90 days
    now = datetime.now().timestamp()
    chartDistance = []
    x = np.empty(shape=(0,0)) #array of dates
    y1 = np.empty(shape=(0,0)) #array of cumulative distances
    volumeSevenHTML = "null"
    rangeDistance=0.0
    for i in range(-90,1):
        rangeDistance = 0.0
        daysAgo = now + 24*3600*i
        cutoff = daysAgo - 24*3600*30
        actions = Action.objects.filter(user=user, type=acttype, postdate__gte=(datetime.fromtimestamp(cutoff)), postdate__lte=(datetime.fromtimestamp(daysAgo)))
        for action in actions:
            rangeDistance += action.distance/1000
        x = np.append(x,datetime.fromtimestamp(daysAgo))
        y1 = np.append(y1, rangeDistance)
    fig, ax = plt.subplots(figsize=(3,3))
    plt.title("30-Day Cumulative "+str(acttype.type)+" Distance")
    plt.ylabel("Distance (km)")
    plt.xlabel("Date")
    ax.plot(x,y1)
    volumeThirtyHTML = mpld3.fig_to_html(fig)
    return(volumeThirtyHTML)


def volumeNinety(userName, acttype):
    user=User.objects.get(username=userName)
    ## Check rolling 90-day cumulative distance over previous 365 days
    now = datetime.now().timestamp()
    chartDistance = []
    x = np.empty(shape=(0,0)) #array of dates
    y1 = np.empty(shape=(0,0)) #array of cumulative distances
    volumeSevenHTML = "null"
    rangeDistance = 0.0
    for i in range(-365,1):
        rangeDistance = 0.0
        daysAgo = now + 24*3600*i
        cutoff = daysAgo - 24*3600*90
        actions = Action.objects.filter(user=user, type=acttype, postdate__gte=(datetime.fromtimestamp(cutoff)), postdate__lte=(datetime.fromtimestamp(daysAgo)))
        for action in actions:
            rangeDistance += action.distance/1000
        x = np.append(x,datetime.fromtimestamp(daysAgo))
        y1 = np.append(y1, rangeDistance)
    fig, ax = plt.subplots(figsize=(3,3))
    plt.title("90-Day Cumulative "+str(acttype.type)+" Distance")
    plt.ylabel("Distance (km)")
    plt.xlabel("Date")
    ax.plot(x,y1)
    volumeNinetyHTML = mpld3.fig_to_html(fig)
    return(volumeNinetyHTML)

def trainingvols(userName, acttype):
    user = User.objects.get(username=userName)
    now = datetime.now().timestamp()
    x1 = np.empty(shape=(0,0)) #array of dates
    y1 = np.empty(shape=(0,0)) #array of cumulative distances
    x2 = np.empty(shape=(0,0)) #array of dates
    y2 = np.empty(shape=(0,0)) #array of cumulative distances
    x3 = np.empty(shape=(0,0)) #array of dates
    y3 = np.empty(shape=(0,0)) #array of cumulative distances
    xlist = []
    ylist = []
    volumeHTML = "null"
    rangeDistance=0.0
    for i in range(-30,0):
        rangeDistance = 0.0
        daysAgo = now + 24*3600*i
        cutoff = daysAgo - 24*3600*7
        actions = Action.objects.filter(user=user, type=acttype, postdate__gte=(datetime.fromtimestamp(cutoff)), postdate__lte=(datetime.fromtimestamp(daysAgo)))
        for action in actions:
            rangeDistance += action.distance/1000
        x1 = np.append(x1,datetime.fromtimestamp(daysAgo))
        xlist.append(x1)
        y1 = np.append(y1, rangeDistance)
        ylist.append(y1)
    rangeDistance=0.0
    for i in range(-90,0):
        rangeDistance = 0.0
        daysAgo = now + 24*3600*i
        cutoff = daysAgo - 24*3600*30
        actions = Action.objects.filter(user=user, type=acttype, postdate__gte=(datetime.fromtimestamp(cutoff)), postdate__lte=(datetime.fromtimestamp(daysAgo)))
        for action in actions:
            rangeDistance += action.distance/1000
        x2 = np.append(x2,datetime.fromtimestamp(daysAgo))
        y2 = np.append(y2, rangeDistance)
        xlist.append(x2)
        ylist.append(y2)
    rangeDistance = 0.0
    for i in range(-365,0):
        rangeDistance = 0.0
        daysAgo = now + 24*3600*i
        cutoff = daysAgo - 24*3600*90
        actions = Action.objects.filter(user=user, type=acttype, postdate__gte=(datetime.fromtimestamp(cutoff)), postdate__lte=(datetime.fromtimestamp(daysAgo)))
        for action in actions:
            rangeDistance += action.distance/1000
        x3 = np.append(x3,datetime.fromtimestamp(daysAgo))
        y3 = np.append(y3, rangeDistance)
        xlist.append(x3)
        ylist.append(y3)
    fig, ax = plt.subplots(1, 3, figsize=(14,2))
    for axi in ax.flat:
        ax[i].plot(xlist[i], ylist[i], 'o-')
    plugins.clear(fig)
    volumeHTML = mpld3.fig_to_html(fig)
    return(volumeHTML)

