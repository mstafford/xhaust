from .models import User, activityType, Activity, Run, Cycle, Hike
from django.conf import settings

import os

runList = Run.objects.all()
cycleList = Cycle.objects.all()
hikeList = Hike.objects.all()
print("Checking for old style runs")
for act in runList:
    type = activityType.objects.get(type='running')
    user = User.objects.get(username=act.user.username)
    activity=Activity(user=user,
        type=type,
        uploaded_at=act.uploaded_at,
        distance=act.distance,
        duration=act.duration,
        title=act.title,
        comments=act.comments,
        posted=act.posted,
        rewards=act.rewards,
        gpsfile=act.gpsfile,
        photo=act.photo,
        photo2=act.photo2,
        photo3=act.photo3,
        pace=act.pace,
        permlink=act.permlink,
        start = act.start,
        verb = 'ran')
    activity.save()
    if not os.path.exists(settings.MEDIA_ROOT+"/documents/"+str(user.pk)+"/"):
        os.makedirs(settings.MEDIA_ROOT+"/documents/"+str(user.pk))
    if activity.gpsfile:
        initial_path = activity.gpsfile.path
        suffix = ".tcx"
        if 'gpx' in activity.gpsfile.name:
            suffix = ".gpx"
        activity.gpsfile.name = "/documents/"+str(user.pk)+"/run_GPS-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.gpsfile.name
        print("Moving GPS file for activity")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo.name == "run-default.jpg":
        pass
    else:
        initial_path = activity.photo.path
        if "gif" in activity.photo.name:
            suffix=".gif"
        else:
            suffix = ".jpg"
        activity.photo.name = "/documents/"+str(user.pk)+"/run_photo1-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo.name
        print("Moving photo#1 for activity!")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo2:
        initial_path = activity.photo2.path
        suffix = ".jpg"
        activity.photo2.name = "/documents/"+str(user.pk)+"/run_photo2-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo2.name
        print("Moving Photo#2 for activity")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo3:
        initial_path = activity.photo3.path
        suffix = ".jpg"
        activity.photo3.name = "/documents/"+str(user.pk)+"/run_photo3-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo3.name
        print("Moving Photo#3 for activity")
        os.rename(initial_path, new_path)
        activity.save()
    act.delete()

print("Checking for old style cycles")
for act in cycleList:
    type = activityType.objects.get(type='cycling')
    user = User.objects.get(username=act.user.username)
    activity=Activity(user=user,
        type=type,
        uploaded_at=act.uploaded_at,
        distance=act.distance,
        duration=act.duration,
        title=act.title,
        comments=act.comments,
        posted=act.posted,
        rewards=act.rewards,
        gpsfile=act.gpsfile,
        photo=act.photo,
        photo2=act.photo2,
        photo3=act.photo3,
        permlink=act.permlink,
        speed=act.speed,
        start=act.start,
        verb='rode')
    activity.save()
    if not os.path.exists(settings.MEDIA_ROOT+"/documents/"+str(user.pk)+"/"):
        os.makedirs(settings.MEDIA_ROOT+"/documents/"+str(user.pk))
    if activity.gpsfile:
        initial_path = activity.gpsfile.path
        suffix = ".tcx"
        if 'gpx' in activity.gpsfile.name:
            suffix = ".gpx"
        activity.gpsfile.name = "/documents/"+str(user.pk)+"/cycle_GPS-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.gpsfile.name
        print("Moving GPS file for activity")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo.name == "cycle-default.jpg":
        pass
    else:
        initial_path = activity.photo.path
        suffix = ".jpg"
        activity.photo.name = "/documents/"+str(user.pk)+"/cycle_photo1-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo.name
        print("Moving photo#1 for activity!")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo2:
        initial_path = activity.photo2.path
        suffix = ".jpg"
        activity.photo2.name = "/documents/"+str(user.pk)+"/cycle_photo2-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo2.name
        print("Moving Photo#2 for activity")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo3:
        initial_path = activity.photo3.path
        suffix = ".jpg"
        activity.photo3.name = "/documents/"+str(user.pk)+"/cycle_photo3-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo3.name
        print("Moving Photo#3 for activity")
        os.rename(initial_path, new_path)
        activity.save()
    act.delete()

print("Checking for old style cycles")
for act in hikeList:
    type = activityType.objects.get(type='hiking')
    user = User.objects.get(username=act.user.username)
    activity=Activity(user=user,
        type=type,
        uploaded_at=act.uploaded_at,
        distance=act.distance,
        duration=act.duration,
        title=act.title,
        comments=act.comments,
        posted=act.posted,
#        rewards=act.rewards,
        gpsfile=act.gpsfile,
        photo=act.photo,
        photo2=act.photo2,
        photo3=act.photo3,
#        pace=act.pace,
        permlink=act.permlink,
        start=act.start,
        verb='hiked')
    activity.save()
    if not os.path.exists(settings.MEDIA_ROOT+"/documents/"+str(user.pk)+"/"):
        os.makedirs(settings.MEDIA_ROOT+"/documents/"+str(user.pk))
    if activity.gpsfile:
        initial_path = activity.gpsfile.path
        suffix = ".tcx"
        if 'gpx' in activity.gpsfile.name:
            suffix = ".gpx"
        activity.gpsfile.name = "/documents/"+str(user.pk)+"/hike_GPS-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.gpsfile.name
        print("Moving GPS file for activity")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo.name == "hike-default.jpg":
        pass
    else:
        initial_path = activity.photo.path
        suffix = ".jpg"
        activity.photo.name = "/documents/"+str(user.pk)+"/hike_photo1-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo.name
        print("Moving photo#1 for activity!")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo2:
        initial_path = activity.photo2.path
        suffix = ".jpg"
        activity.photo2.name = "/documents/"+str(user.pk)+"/hike_photo2-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo2.name
        print("Moving Photo#2 for activity")
        os.rename(initial_path, new_path)
        activity.save()
    if activity.photo3:
        initial_path = activity.photo3.path
        suffix = ".jpg"
        activity.photo3.name = "/documents/"+str(user.pk)+"/hike_photo3-"+str(activity.pk)+str(suffix)
        new_path = settings.MEDIA_ROOT + activity.photo3.name
        print("Moving Photo#3 for activity")
        os.rename(initial_path, new_path)
        activity.save()
    act.delete()


