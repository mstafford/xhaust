from activities.models import Activity, User, Action
from django.contrib.auth.models import User as SteemUser

def copy_activities():
    activityList = Activity.objects.all()
    for act in activityList:
        user = act.user
        action=Action(user=SteemUser.objects.get(username=user.username),
            type = act.type,
            posted = act.posted,
            title = act.title,
            comments = act.comments,
            gpsfile =act.gpsfile,
            photo = act.photo,
            photo2 = act.photo2,
            photo3 = act.photo3,
            uploaded_at = act.uploaded_at,
            postdate = act.postdate,
            ascent = act.ascent,
            descent = act.descent,
            distance = act.distance,  ##length in metres
            duration =  act.duration, ##time in seconds
            pace = act.pace,
            permlink = act.permlink,
            rewards = act.rewards,
            steps = act.steps,
            start = act.start,
            speed = act.speed,
            verb = act.verb
        )
        print(act.title, action.title)
        action.save()


