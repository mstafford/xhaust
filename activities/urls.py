from django.conf.urls import url, include
from django.urls import path

from jchart.views import ChartView

from markdownx import urls as markdownx

from . import charts
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('dash/', views.dash, name='dash'),
    path('new/<str:activity_type>', views.test_new_upload, name='upload'),
    path('delete/<int:activity>', views.delete_activity, name='delete'),
    path('feed/<str:activity_type>/<int:activity_id>/', views.gps_detail, name='detail-other'),
    path('feed/<str:activity_type>/<str:sort>/', views.feed, name='feed'),
    path('@<str:steem_id>/', views.browse, name='browse'),
    path('vote/@<str:author>/<str:comment>/w<int:weight>', views.vote_comment, name='votec'),
    path('vote/a<int:activity_id>/w<int:weight>', views.vote_activity, name='votep'),
    path('leaderboard', views.leaderboard, name='leader'),
    path('events/<str:activity_type>/', views.events, name='events'),
    path('event/<int:event_id>/', views.event_detail, name='eventdetail'),
    path('event/commit/<int:event_id>/', views.join_or_leave_event, name="joinleave"),
    path('post/event/<int:event_id>/', views.event_post, name="eventpost"),
    path('dash/<str:activity_type>/<int:activity_id>/', views.gps_detail, name='detail-self'),
    path('dash/<str:activity_type>/<int:activity_id>/post/', views.create_post, name='post'),
    path('comment/@<str:pauthor>/<str:plink>/', views.createComment, name='comment'),
    path('feed/run/<int:run_id>/share/', views.share_post, name='share'),
    path('article/@<str:steem_id>/<slug:link>', views.article, name='article'),
    path('test/', views.test_feat, name='newfeature'),
    path('settings/<str:target>/<int:level>/', views.map_privacy, name='map-settigns'),
    url(r'^signup/$', views.SignUpView.as_view(), name='signup'),
    url(r'^votecalc$', views.votecalc, name='votecalc'),
    url(r'^updatedash$', views.update_dash, name='changeact'),
]
